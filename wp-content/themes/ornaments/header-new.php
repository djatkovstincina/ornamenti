<?php
/**
 * @package ornaments
 */
global $globalSite;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="<?php echo get_template_directory_uri() . '/bundles/images/favicon.ico'; ?>" type="image/x-icon" />

    <?php wp_head(); ?>
</head>

<body <?php body_class('loading'); ?>>
<?php
$site_logo = get_field('site_logo', 'option');
?>
<div id="page" class="site">
    <div class="m-overlay"></div>
    <header id="masthead" class="site-header" role="banner">
        <div class="wrapper">
            <div class="wrap top-header">
                <a href="<?php _e( home_url( '/' ) ); ?>" id="brand" class="site-logo">
                    <?php
                    if ($site_logo): echo '<img src="' . $site_logo . '" >';
                    else: echo '3D Ornaments';
                    endif; ?>
                </a>
                <div class="right-nav ls-d">
                    <div class="search-trigger"><a href="javascript:void(0);"><i class="fa fa-search"></i></a></div>
                </div>
                <?php
//                <div class="lang-switch"><?php do_action('wpml_add_language_selector'); '?'></div>
            ?>
                <div class="top-menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu' => 'top-menu' ) ); ?>
                </div>
                <div class="_12 header-search-form">
                    <form role="search" class="header-form search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                        <div class="search-elements">
                            <label class="screen-reader-text" for="q">Search</label>
                            <input type="search" class="header-form-search" placeholder="Type keyword and press Enter to search" value="" name="s">
                            <input type="submit" class="search-submit" value="Search">
                        </div>
                    </form>
                </div>
                <div class="right-nav ls-m">
                    <div class="search-trigger"><a href="javascript:void(0);"><i class="fa fa-search"></i></a></div> |
                </div>
                <button id="menu-button" class="site-menu-button v2">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
        </div>
        <div class="wrapper-fluid">
            <div class="wrap bottom-header">
                <nav id="site-navigation" class="site-navigation" role="navigation">
                    <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
                </nav>
            </div>
        </div>
    </header>
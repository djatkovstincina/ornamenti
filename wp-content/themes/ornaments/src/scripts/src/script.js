/* eslint-disable no-console */

(function ($) {
    'use strict';
    /**
     * Global Variables
     */
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    /**
     * Returns a function, that, as long as it continues to be invoked, will not
     * be triggered. The function will be called after it stops being called for
     * N milliseconds. If `immediate` is passed, trigger the function on the
     * leading edge, instead of the trailing.
     * @param {fn} func - function to debounce
     * @param {number} wait - time to wait
     * @param {bool} immediate
     * @returns {Function}
     */
    var debounce = function (func, wait, immediate) {
        var timeout;
        var waitTime = wait || 100;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                timeout = null;
                if (!immediate) {
                    func.apply(context, args);
                }
            }, waitTime);
            if (immediate && !timeout) {
                func.apply(context, args);
            }
        };
    };
    /**
     * Collection of useful site functions
     * @type {{init: init, smoothScroll: smoothScroll}}
     */
    var siteFunctions = {
        init: function () {
            siteFunctions.smoothScroll();
        },
        /**
         * Smooth Scroll function for anchor clicks
         */
        smoothScroll: function () {
            $('a[href*="#"]:not(.no-scroll)').click(function () {
                var target = $(this.hash);
                if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').stop().animate({
                            scrollTop: target.offset().top - 0
                        }, 1000);
                        return false;
                    }
                }
            });
        }
    };

    var siteHeader = {
        init: function () {

        },
        /**
         * Handle on scroll header functionality
         */
        scrollChange: function () {
            var $body = $('body');
            $(document).scrollTop() > 50 ? $body.addClass('scroll') : $body.removeClass('scroll'); //jshint ignore:line
        }
    };

    var siteMenu = {
        prevent: false,
        init: function () {
            /**
             * Toggle menu with clicking on hamburger menu and overlay
             */
            $('#menu-button, .m-overlay').click(function (e) {
                e.preventDefault();
                siteMenu.toggle();
            });
        },
        toggle: function () {
            if (!siteMenu.prevent) {
                $('body').toggleClass('m-open');
                siteMenu.prevent = !siteMenu.prevent;

                setTimeout(function () {
                    siteMenu.prevent = !siteMenu.prevent;
                }, 400);
            }
        }
    };

    var slickSliders = {

        init: function () {
            slickSliders.homeHeaderSlider();
            slickSliders.settingSlider();
            slickSliders.similarProductsSlider();
            slickSliders.productSlider();
            slickSliders.gallery();
        },

        homeHeaderSlider: function () {
            var windowWidth = window.innerWidth;
            var homeHeaderSlider = $('.header-slider');
            var PROPERTIES = {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 14000,
                dots: true,
                infinite: true,
                arrows: false,
                pauseOnFocus: false,
                pauseOnHover: false,
                slide: '.header-slide',
                speed: 1000,
                fade: true,
                cssEase: 'linear',
                prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false
                        }
                    }
                ]
            };
            homeHeaderSlider.slick(PROPERTIES);

            homeHeaderSlider.on('afterChange', function (event, slick, currentSlide) {
                var prevSlideIndex = currentSlide - 1;
                var prevSlide = $('.header-slider [data-slick-index = ' + prevSlideIndex + ']');
                var innerImage = $(prevSlide).find('.inner-image');

                $(innerImage).removeClass('animated-image');

                setTimeout(function () {
                    $(innerImage).addClass('animated-image');
                }, 50);
            });

            if (windowWidth > 767) {

                setTimeout(function () {
                    homeHeaderSlider.slick('slickGoTo', 0);
                }, 950);
            }
        },

        settingSlider: function () {

            $('.gallery-wrap').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                speed: 700,
                slide: '.image',
                cssEase: 'linear',
                prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            arrows: true
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            arrows: false
                        }
                    }
                ]
            });
        },

        similarProductsSlider: function () {

            $('.similar-products').slick({
                cssEase: 'linear',
                centerMode: true,
                infinite: true,
                autoplaySpeed: 0,
                speed: 5000,
                easing:'linear',
                pauseOnHover: true,
                swipeToSlide: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                draggable: true,
                arrows: false,
                slide: '.similar-single',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            arrows: false
                        }
                    }
                ]
            });
        },

        productSlider: function () {
            var prodSlider = $('.images-wrap');
            var OPTIONS = {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                infinite: true,
                adaptiveHeight: true,
                speed: 1000,
                asNavFor: '.thumbs',
                fade: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                        }
                    }
                ]
            };

            prodSlider.each(function (index, item) {
                var _item = $(item);
                var _slickItem = _item.find('.js-main-image');
                var _slickThumbs = _item.find('.thumbs');
                var _slickThumbsPop = _item.find('.thumbs-popup');

                _slickItem.slick(OPTIONS);

                _slickThumbs.slick({
                    easing:'linear',
                    pauseOnHover: true,
                    autoplay: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: _slickItem,
                    dots: true,
                    arrows: false,
                    draggable: true,
                    infinite: true,
                    speed: 3000,
                    focusOnSelect: true,
                    responsive: [
                        {
                            breakpoint: 991,
                            settings: {
                                slidesToShow: 2
                            }
                        }
                    ]
                });

                _slickThumbsPop.slick({
                    easing:'linear',
                    pauseOnHover: true,
                    autoplay: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    asNavFor: _slickItem,
                    adaptiveHeight: true,
                    dots: false,
                    arrows: true,
                    draggable: true,
                    infinite: true,
                    speed: 2000,
                    focusOnSelect: true
                });
            });
        },

        gallery: function () {

            $('.images-wrap').slick({
                cssEase: 'linear',
                infinite: true,
                swipeToSlide: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                draggable: true,
                arrows: true,
                slide: '.gallery-image',
                adaptiveHeight: true,
            });
        }
    };

    var selectStyle = {

        init: function () {
            selectStyle.functionality();
        },

        functionality: function () {
            $('select').each(function () {
                var $this = $(this),
                    numberOfOptions = $(this).children('option').length;

                $this.addClass('select-hidden');
                $this.wrap('<div class="select"></div>');
                $this.after('<div class="select-styled"></div>');

                var $styledSelect = $this.next('div.select-styled');
                $styledSelect.text($this.children('option:selected').text());

                var $list = $('<ul />', {
                    'class': 'select-options'
                }).insertAfter($styledSelect);

                for (var i = 0; i < numberOfOptions; i++) {
                    $('<li />', {
                        text: $this.children('option').eq(i).text(),
                        rel: $this.children('option').eq(i).val()
                    }).appendTo($list);
                }

                var $listItems = $list.children('li');

                $styledSelect.click(function (e) {
                    e.stopPropagation();
                    $('div.select-styled.active').not(this).each(function () {
                        $(this).removeClass('active').next('ul.select-options').slideUp();
                    });
                    $(this).toggleClass('active').next('ul.select-options').slideDown();
                });

                $listItems.click(function (e) {
                    e.stopPropagation();
                    $styledSelect.text($(this).text()).removeClass('active');
                    $this.val($(this).attr('rel'));
                    $list.slideUp();
                });

                $(document).click(function () {
                    $styledSelect.removeClass('active');
                    $list.slideUp();
                });

            });
        },
    };

    var masonryCards = {

        init: function () {
            masonryCards.columns();
        },

        columns: function () {
            var grid = $('.ref-grid').masonry({
                itemSelector: '.reference',
                horizontalOrder: true,
                columnWidth: '.reference'
            });

            $('#loadCards').click(function (e) {
                e.preventDefault();
                $('.ref-grid, .mark-grid').find('.hide:lt(9)').animate({opacity: 1}, 200, function () {
                    $(this).removeClass('hide');
                    grid.masonry();
                });
            });
        },
    };

    var fancyBox = {

        init: function () {
            fancyBox.popup();
        },

        popup: function () {
            $('a.i-link, .gallery .image, .poster').fancybox({
                buttons: [
                    'zoom',
                    'share',
                    'slideShow',
                    'fullScreen',
                    'thumbs',
                    'close'
                ],
                loop: true,
                thumbs: {
                    autoStart: true,
                    axis: 'x'
                },
                afterLoad : function() {
                    $('.thumbs').slick('refresh');
                    $('.thumbs-popup').slick('refresh');
                }
            });

        },
    };

    var showHide = {

        init: function () {
            showHide.openAnswer();
            showHide.openDetails();
        },

        openAnswer: function () {
            var prevent = false;
            var trigger;
            var answer;

            $('.q-part').click(function () {
                trigger = $(this).data('value');
                answer = $('.faq').find('.answer[data-value=' + trigger + ']');

                if (!prevent && !answer.is(':visible')) {
                    $('.q-part').removeClass('active');
                    $(this).addClass('active');
                    prevent = !prevent;

                    $('.answer').slideUp();
                    answer.delay().slideDown();

                    setTimeout(function () {
                        prevent = !prevent;
                    }, 400);
                }
            });
        },

        openDetails: function () {
            var prevent = false;
            var trigger;
            var details;

            $('.js-details').click(function () {
                trigger = $(this).data('value');
                details = $('.js-slider-wrap').find('.description[data-value=' + trigger + ']');

                if (!prevent && !details.is(':visible')) {
                    $('.js-details').removeClass('active');
                    $(this).addClass('active');
                    prevent = !prevent;

                    $('.description').slideUp(300);
                    details.delay(300).slideDown();

                    setTimeout(function () {
                        prevent = !prevent;
                    }, 400);
                }
            });
        },
    };

    var toTop = {
        init: function () {
            toTop.back();
        },

        back: function () {
            var btn = $('#toTop');

            $(window).scroll(function () {
                if ($(window).scrollTop() > 800) {
                    btn.addClass('show');
                } else {
                    btn.removeClass('show');
                }
            });

            btn.on('click', function (e) {
                e.preventDefault();
                $('html, body').animate({scrollTop: 0}, '300');
            });
        },
    };

    var loader = {
        init: function () {
            loader.logo();
        },

        logo: function () {
            $('#loader').delay(900).fadeOut(900);
        },

    };

    var tabs = {

        init: function () {
            tabs.show();
        },

        show: function () {

            $('.tabs .tab:not(:first)').addClass('inactive');
            $('.container').hide();
            $('.container:first').show();

            $('.tabs .tab').click(function () {
                var t = $(this).attr('data-attribute');
                if ($(this).hasClass('inactive')) { //this is the start of our condition
                    $('.tabs .tab').addClass('inactive');
                    $(this).removeClass('inactive');

                    $('.container').hide();
                    $('#' + t).fadeIn('slow');
                }
            });
        },
    };

    var submenu = {

        init: function () {
            submenu.show();
        },

        show: function () {

            var menu = $('.menu-item-has-children');
            var submenu = $('.menu-item-has-children .sub-menu');

            submenu.removeClass('active');

            menu.hover(function () {
                $(this).find('.sub-menu').toggleClass('active');
            });
        },
    };

    var mobileMenu = {

        init: function () {
            mobileMenu.open();
        },

        open: function () {
            $('#primary-menu .menu-item-has-children').append('<span class="open-menu fa fa-plus"></span>');

            $('#primary-menu > .menu').wrap('<div class="overflow"></div>');
            $(window).on('load resize', function () {
                var vph = $(window).height() - 57;
                $('.overflow').css('max-height', vph);
            });

            $('.open-menu').on('click', function (e) {
                e.preventDefault();
                $(this).prev('ul').slideToggle(250);
                $(this).toggleClass('rotate');
                $(this).children().toggleClass('fa-reorder fa-remove');
            });
        },
    };

    var videoPlay = {

        init: function () {
            videoPlay.playHover();
        },

        playHover: function () {
            var figure = $('.js-video');

            [].forEach.call(figure, function(item) {
                item.addEventListener('mouseover', hoverVideo, false);
                item.addEventListener('mouseout', hideVideo, false);
            });

            function hoverVideo() {
                $('video', this).get(0).play();
            }

            function hideVideo() {
                $('video', this).get(0).pause();
            }
        },
    };


    $(document).ready(function () {
        siteFunctions.init();
        siteMenu.init();
        fancyBox.init();
        showHide.init();
        toTop.init();
        selectStyle.init();
        tabs.init();
        submenu.init();
        mobileMenu.init();
        videoPlay.init();
        slickSliders.init();

        $(window).scroll(debounce(function () {
            siteHeader.scrollChange();
        }));
    });

    $(window).on('load', function () {
        loader.init();
    });

    //Header search form
    $(function () {
        $('.right-nav > .search-trigger, .header-search-form > .close-search').click(function (e) {
            e.stopPropagation();
            $('.header-search-form').slideDown().toggleClass('active');
            $('.header-form-search').focus();
        });

        $(document).click(function (event) {
            if (!$(event.target).closest('.header-search-form').length) {
                if ($('.header-search-form').is(':visible')) {
                    $('.header-search-form').slideUp().removeClass('active');
                }
            }
        });

    });

}(jQuery));

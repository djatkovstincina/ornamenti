(function ($) {

    var verticalScroll = {

        init: function () {
            verticalScroll.ideas();
        },

        ideas: function () {

            $.scrollify({
                section: '.js-slide',
                interstitialSection: '.entering, .site-footer',
                easing: 'easeOutExpo',
                scrollSpeed: 1600,
                offset: 0,
                scrollbars: true,
                standardScrollElements: '',
                setHeights: true,
                overflowScroll: true,
                updateHash: true,
                touchScroll: true,
                before: function () {
                },
                after: function () {
                },
                afterResize: function () {
                },
                afterRender: function () {
                }
            });
        },
    };

    $(document).ready(function () {
        verticalScroll.init();
    });

}(jQuery));
<?php
/**
 * The template for displaying all custom product posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ornaments
 */

get_header(); ?>
    <div id="content" class="site-content">
        <?php
        $term = get_queried_object();
        $header_height = get_field('h_height', $term);
        $random_image = get_template_directory_uri() . '/src/images/random.jpg';
        $header_control = get_field('h_control', $term);
        $header_video_mp4 = get_field('h_mp4', $term);
        $header_video_webm = get_field('h_webm', $term);
        $header_video_ogv = get_field('h_ogv', $term);
        $header_image_num = get_field('h_slider', $term);
        $header_external = get_field('h_external', $term);
        $embed = get_field('external_video', $term);
        if ( $embed ) {
            preg_match('/src="(.+?)"/', $embed, $matches);
            $src = $matches[1];
            $params = array(
                'controls'    => 1,
                'hd'        => 1,
                'rel' => 0,
                'autohide'    => 1,
                'autoplay' => 0,
                'showinfo' => 0,
            );
            $new_src = add_query_arg($params, $src);
            $embed = str_replace($src, $new_src, $embed);
            $attributes = 'frameborder="0"';
            $embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $embed);
        }
        $count = $header_image_num; ?>

        <section id="header-section" class="header-section<?php if ( !$header_control ) echo ' wrapper-fluid'; else echo ' wrapper'; if ( $header_height === 'Full Screen' ) echo ' full'; ?>">
            <?php
            if ( $header_control ) :
                if ( $header_external ) : ?>
                    <div class="wrap video-container -external">
                        <?php echo $embed; ?>
                    </div>
                <?php
                else: ?>
                    <div class="wrap video-container">
                        <video id="header-video" data-object="video" class="site-header-video" poster="<?php echo $random_image; ?>" autoplay playsinline muted loop>
                            <?php
                            if ( $header_video_ogv ) : ?>
                                <source src="<?php echo $header_video_ogv; ?>" type="video/ogv">
                            <?php
                            endif;
                            if ( $header_video_webm ) : ?>
                                <source src="<?php echo $header_video_webm; ?>" type="video/webm">
                            <?php
                            endif; ?>
                            <source src="<?php echo $header_video_mp4; ?>" type="video/mp4">
                        </video>
                    </div>
                <?php
                endif;
            elseif ( have_rows('h_slider', $term) ): ?>
                <div class="wrap header-slider border-img">
                    <?php while ( have_rows('h_slider', $term) ) : the_row();
                        $header_image = get_sub_field('h_image', $term);
                        $black = get_sub_field('bb', $term);
                        $header_title = get_sub_field('h_title', $term);
                        $header_subtitle = get_sub_field('h_subtitle', $term); ?>
                        <div class="header-slide <?php if($count==1) echo 'inner'; ?>" data-image="<?php echo $header_image; ?>" style="background-image: url(<?php echo $header_image; ?>);">
                            <div class="inner-image animated-image" style="background-image: url(<?php echo $header_image; ?>);"></div>

                            <div class="wrapper">
                                <div class="header-info _12 <?php if($black) echo 'black'; ?>">
                                    <?php
                                    if ( $header_title ) : ?>
                                        <h1 class="header-title">
                                            <?php
                                            echo $header_title; ?>
                                        </h1>
                                    <?php
                                    endif;
                                    if ( $header_subtitle ) : ?>
                                        <p class="header-subtitle">
                                            <?php
                                            echo $header_subtitle; ?>
                                        </p>
                                    <?php
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile; ?>
                </div>
            <?php
            elseif( has_post_thumbnail() ): ?>
                <div class="wrap header-slider">
                    <div class="header-slide" data-image="<?php the_post_thumbnail_url('large'); ?>" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>);">
                        <div class="inner-image animated-image" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>);"></div>
                    </div>
                </div>
            <?php
            else : ?>
                <div class="wrap header-image" style="background-image: url(<?php echo $random_image; ?>);"></div>
            <?php
            endif;
            ?>
        </section>
        <div class="section-social">
            <div class="header-social">
                <?php echo ornaments_social_network(); ?>
            </div>
        </div>
        <main id="main" class="page-main site-main" role="main">
            <?php
            the_breadcrumb(); ?>
            <section class="section products-archive">
                <div class="wrapper">
                    <div class="wrap">
                        <?php
                        if (have_posts()) :
                            while (have_posts()) : the_post();
                                get_template_part('template-parts/post/content', 'product');
                            endwhile;
                        else :
                            get_template_part('template-parts/post/content', 'none');
                        endif; ?>
                    </div>
                    <div class="wrap">
                        <?php ornaments_nav(); ?>
                    </div>
                </div>
            </section>
            <?php
            if (have_rows('blocks_gallery')): ?>
                <section class="section gallery">
                    <div class="wrapper-fluid">
                        <?php while (have_rows('blocks_gallery')) : the_row();
                            $title = get_sub_field('title');
                            $text = get_sub_field('text'); ?>

                            <div class="wrap ing-wrap">
                            <div class="_12 _m4 title-wrap">
                                <h4 class="title"><?php echo $title; ?></h4>
                                <div class="text"><?php echo $text; ?></div>
                            </div>
                            <?php if (have_rows('images_gallery')): ?>
                                <div class="_12 _m8 images-wrap">
                                    <?php while (have_rows('images_gallery')) : the_row();
                                        $image = get_sub_field('image'); ?>
                                        <img class="image" src="<?php echo $image; ?>"/>
                                    <?php endwhile; ?>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </section>
            <?php
            endif;

            $section_title = get_field('section_title');
            if (have_rows('card_repeater')): ?>
                <section class="section categories">
                    <div class="wrapper-fluid">
                        <div class="wrap">
                            <h2 class="categories-title _12 _m8 ofs_m2"><?php if ($section_title) echo $section_title; ?></h2>
                        </div>
                        <div class="wrap">
                            <?php while (have_rows('card_repeater')) : the_row();
                                $card_image = get_sub_field('card_image');
                                $card_title = get_sub_field('card_title');
                                $card_link = get_sub_field('card_link'); ?>

                                <a href="<?php echo $card_link; ?>" class="link">
                                    <div class="category-image"
                                         style="background-image: url('<?php echo $card_image; ?>')">
                                        <div class="title-wrap">
                                            <h4 class="title"><?php echo $card_title; ?></h4>
                                        </div>
                                    </div>
                                </a>
                            <?php
                            endwhile; ?>
                        </div>
                    </div>
                </section>
            <?php
            endif; ?>
            <?php
            get_template_part('template-parts/layout/layout', 'newsletter');
            get_template_part('template-parts/layout/layout', 'links-banner');
            ?>
        </main>
    </div>
<?php
get_footer();
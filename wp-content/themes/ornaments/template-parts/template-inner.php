<?php
/* Template Name: Inner Page */
get_header(); ?>
    <div id="content" class="site-content">
        <?php
        headerPage(); ?>
        <main id="main" class="page-main site-main" role="main">
            <?php
            the_breadcrumb(); ?>

            <section class="section page-title">
                <div class="wrapper">
                    <div class="wrap">
                        <h1 class="_12 _m10 ofs_m1 title"><?php echo get_the_title(); ?></h1>
                    </div>
                </div>
            </section>

            <?php
            //          Start Inner Page section
            if (have_rows('content-inner')) : ?>

                <section class="section inner-page">
                    <div class="wrapper">
                    <?php while (have_rows('content-inner')) : the_row(); ?>
                            <?php
                            if (get_row_layout() == 'g_o_h') :
                                get_template_part('template-parts/layout/gallery/gallery', 'one-horizontal');
                            elseif (get_row_layout() == 'g_t_h') :
                                get_template_part('template-parts/layout/gallery/gallery', 'two-horizontal');
                            elseif (get_row_layout() == 'g_th_h') :
                                get_template_part('template-parts/layout/gallery/gallery', 'three-horizontal');
                            elseif (get_row_layout() == 'g_f_h') :
                                get_template_part('template-parts/layout/gallery/gallery', 'four-horizontal');
                            elseif (get_row_layout() == 'i_t') :
                                get_template_part('template-parts/layout/layout', 'inner-text');
                            elseif (get_row_layout() == 'btn') :
                                get_template_part('template-parts/layout/layout', 'button');
                            elseif (get_row_layout() == 'vd') :
                                get_template_part('template-parts/layout/layout', 'video');
                            elseif (get_row_layout() == 'lr') :
                                get_template_part('template-parts/layout/layout', 'image-text-lr');
                            endif; ?>
                    <?php endwhile; ?>
                    </div>
                </section>
                <?php
                if (have_rows('blocks_gallery')): ?>
                    <section class="section gallery">
                        <div class="wrapper-fluid">
                            <?php while (have_rows('blocks_gallery')) : the_row();
                                $title = get_sub_field('title');
                                $text = get_sub_field('text'); ?>

                                <div class="wrap ing-wrap">
                                    <div class="_12 _m4 title-wrap">
                                        <h4 class="title"><?php echo $title; ?></h4>
                                        <div class="text"><?php echo $text; ?></div>
                                    </div>
                                    <?php if (have_rows('images_gallery')): ?>
                                    <div class="_12 _m8 images-wrap">
                                        <?php while (have_rows('images_gallery')) : the_row();
                                            $image = get_sub_field('image'); ?>
                                            <img class="gallery-image" src="<?php echo $image; ?>"/>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </section>
                <?php
                endif;

                $section_title = get_field('section_title');
                if( have_rows('card_repeater') ): ?>
                    <section class="section categories">
                        <div class="wrapper-fluid">
                            <div class="wrap">
                                <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
                            </div>
                            <div class="wrap">
                                <?php while ( have_rows('card_repeater') ) : the_row();
                                    $card_image = get_sub_field('card_image');
                                    $card_title = get_sub_field('card_title');
                                    $card_link = get_sub_field('card_link'); ?>

                                    <a href="<?php echo $card_link; ?>" class="link">
                                        <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                                            <div class="title-wrap">
                                                <h4 class="title"><?php echo $card_title; ?></h4>
                                            </div>
                                        </div>
                                        <span class='line-1'></span>
                                        <span class='line-2'></span>
                                        <span class='line-3'></span>
                                        <span class='line-4'></span>
                                    </a>
                                <?php
                                endwhile; ?>
                            </div>
                        </div>
                    </section>
                <?php
                endif; ?>

            <?php endif;
            //          End Inner Page section
            get_template_part('template-parts/layout/layout', 'newsletter');
            get_template_part('template-parts/layout/layout', 'links-banner');
            ?>
        </main>
    </div>
<?php
get_footer();
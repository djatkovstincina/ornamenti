<?php
/* Template Name: Contact */
get_header(); ?>
    <div id="content" class="site-content">
        <?php
        headerPage(); ?>
        <main id="main" class="page-main site-main" role="main">
            <?php
            the_breadcrumb(); ?>
            <div class="contact-wrapper">
                <?php
                //          Start Ask a Q section
                $ask_image = get_field('ask_image');  ?>
                <section class="section ask">
                    <div class="wrapper">
                        <div class="wrap">
                            <div class="_12">
                                <div class="c-image" style="background-image: url('<?php echo $ask_image; ?>')">
                                    <div class="box">
                                        <h2 class="ask-title"><i class="fa fa-calculator"></i> Ask for a quote</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
                //          End Ask a Q section

                //          Start Location Info section
                $ci = get_field('ci');  ?>
                    <section class="section location-info">
                        <div class="wrapper">
                            <div class="wrap">
                                <div class="_12">
                                    <div class="info">
                                        <?php echo $ci; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php
                //          End Location Info section

                //          Start Contact Form section
                $cft = get_field('cfsh'); ?>

                <section class="section contact-form">
                    <div class="wrapper">
                        <div class="wrap">
                            <h1 class="_12 _m10 ofs_m1 title"><?php if ($cft) echo $cft; else _e('Send Us a Message', 'ornaments'); ?></h1>
                        </div>
                        <div class="wrap">
                            <div class="_12">
                            <?php if (get_field('cfsh')):
                                echo get_field('cfsh');
                            else:
                                echo do_shortcode('[contact-form-7 id="5" title="Contact Page Form"]');
                            endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php
            if (have_rows('blocks_gallery')): ?>
                <section class="section gallery">
                    <div class="wrapper-fluid">
                        <?php while (have_rows('blocks_gallery')) : the_row();
                            $title = get_sub_field('title');
                            $text = get_sub_field('text'); ?>

                            <div class="wrap ing-wrap">
                            <div class="_12 _m4 title-wrap">
                                <h4 class="title"><?php echo $title; ?></h4>
                                <div class="text"><?php echo $text; ?></div>
                            </div>
                            <?php if (have_rows('images_gallery')): ?>
                                <div class="_12 _m8 images-wrap">
                                    <?php while (have_rows('images_gallery')) : the_row();
                                        $image = get_sub_field('image'); ?>
                                        <img class="gallery-image" src="<?php echo $image; ?>"/>
                                    <?php endwhile; ?>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </section>
            <?php
            endif;

            $section_title = get_field('section_title');
            if( have_rows('card_repeater') ): ?>
                <section class="section categories">
                    <div class="wrapper-fluid">
                        <div class="wrap">
                            <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
                        </div>
                        <div class="wrap">
                            <?php while ( have_rows('card_repeater') ) : the_row();
                                $card_image = get_sub_field('card_image');
                                $card_title = get_sub_field('card_title');
                                $card_link = get_sub_field('card_link'); ?>

                                <a href="<?php echo $card_link; ?>" class="link">
                                    <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                                        <div class="title-wrap">
                                            <h4 class="title"><?php echo $card_title; ?></h4>
                                        </div>
                                    </div>
                                    <span class='line-1'></span>
                                    <span class='line-2'></span>
                                    <span class='line-3'></span>
                                    <span class='line-4'></span>
                                </a>
                            <?php
                            endwhile; ?>
                        </div>
                    </div>
                </section>
            <?php
            endif; ?>
            <?php
            //          End Contact Form section
            get_template_part('template-parts/layout/layout', 'newsletter');
            get_template_part('template-parts/layout/layout', 'links-banner');
            ?>
        </main>
    </div>
<?php
get_footer();

<?php
/* Template Name: Product Categories */
get_header(); ?>
    <div id="content" class="site-content">
        <?php
        headerPage(); ?>
        <main id="main" class="page-main site-main" role="main">
            <?php the_breadcrumb(); ?>
            <?php
            //          Product slider section
            $products_title = get_field('products_title');
            $products_subtitle = get_field('products_subitle');
            $products_description = get_field('products_description');
            ?>

            <section class="section products">
                <div class="wrapper-fluid">
                    <div class="wrap">
                        <h1 class="_12 _m10 ofs_m1 section-title"><?php echo $products_title; ?></h1>
                        <p class="_12 _m10 ofs_m1 section-subtitle"><?php echo $products_subtitle; ?></p>
                    </div>
                        <?php
                        if( have_rows('products_repeater') ):
                            while ( have_rows('products_repeater') ) : the_row();
                                $terms = get_sub_field('product_category');
                                $count = 0;
                                if (is_array($terms)) { $count = count($terms); }
                            endwhile;
                        endif;?>
                        <div class="wrap grid <?php if($count <= 4) echo 'four'; elseif ($count <= 8) echo 'eight'; elseif ($count <= 12) echo 'twelve'; ?>">
                        <?php
                        if( have_rows('products_repeater') ):
                            while ( have_rows('products_repeater') ) : the_row();
                                $terms = get_sub_field('product_category');
                                if( $terms ):
                                    foreach( $terms as $term ): ?>
                                    <?php
                                        $post_args = array(
                                            'posts_per_page' => 1,
                                            'post_type' => 'product',
                                            'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'product_category',
                                                    'field' => 'term_id',
                                                    'terms' => $term->term_id,
                                                )
                                            )
                                        );
                                        $myposts = get_posts($post_args);
                                        foreach ( $myposts as $post ) :
                                            setup_postdata( $post);
                                            $images = get_field('image_repeater', $post->ID);
                                            $product_image = $images[0]['product_image']; ?>
                                        <?php endforeach; ?>
                                        <?php wp_reset_postdata(); ?>

                                        <a class="_12 link -cat" href="<?php echo get_term_link($term->slug, 'product_category'); ?>">
                                            <div class="product_category" style="background-image: url('<?php echo $product_image; ?>');">
                                            </div>
                                            <div class="title-wrap">
                                                <h2 class="title"><?php echo $term->name; ?></h2>
                                            </div>
                                            <div class="desc-wrap">
                                                <p class="desc"><?php echo $term->description; ?></p>
                                            </div>
                                        </a>
                                    <?php endforeach; ?>
                                <?php endif;
                            endwhile;
                        endif; ?>
                    </div>
                </div>
            </section>
            <?php
            if (have_rows('blocks_gallery')): ?>
                <section class="section gallery">
                    <div class="wrapper-fluid">
                        <?php while (have_rows('blocks_gallery')) : the_row();
                            $title = get_sub_field('title');
                            $text = get_sub_field('text'); ?>

                            <div class="wrap ing-wrap">
                            <div class="_12 _m4 title-wrap">
                                <h4 class="title"><?php echo $title; ?></h4>
                                <div class="text"><?php echo $text; ?></div>
                            </div>
                            <?php if (have_rows('images_gallery')): ?>
                                <div class="_12 _m8 images-wrap">
                                    <?php while (have_rows('images_gallery')) : the_row();
                                        $image = get_sub_field('image'); ?>
                                        <img class="gallery-image" src="<?php echo $image; ?>"/>
                                    <?php endwhile; ?>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </section>
            <?php
            endif;

            $section_title = get_field('section_title');
            if( have_rows('card_repeater') ): ?>
                <section class="section categories">
                    <div class="wrapper-fluid">
                        <div class="wrap">
                            <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
                        </div>
                        <div class="wrap">
                            <?php while ( have_rows('card_repeater') ) : the_row();
                                $card_image = get_sub_field('card_image');
                                $card_title = get_sub_field('card_title');
                                $card_link = get_sub_field('card_link'); ?>

                                <a href="<?php echo $card_link; ?>" class="link">
                                    <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                                        <div class="title-wrap">
                                            <h4 class="title"><?php echo $card_title; ?></h4>
                                        </div>
                                    </div>
                                    <span class='line-1'></span>
                                    <span class='line-2'></span>
                                    <span class='line-3'></span>
                                    <span class='line-4'></span>
                                </a>
                            <?php
                            endwhile; ?>
                        </div>
                    </div>
                </section>
            <?php
            endif; ?>
            <?php
            //          End Contact Form section
            get_template_part('template-parts/layout/layout', 'newsletter');
            get_template_part('template-parts/layout/layout', 'links-banner');
            ?>
        </main>
    </div>
<?php
get_footer();

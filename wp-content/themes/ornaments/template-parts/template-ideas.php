<?php
/* Template Name: Interior Ideas */

$enb_image = get_field('enb_image');
$link_text = get_field('link_text');

get_header('clear');

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
} ?>
    <div id="content" class="site-content">
        <main id="main" class="page-main site-main" role="main">
            <div class="rotate">
                <i class="fa fa-undo-alt"></i>
                <p>Rotate your device</p>
            </div>
            <div class="section entering">
                <div class="section-social vertical">
                    <div class="header-social">
                        <?php echo ornaments_social_network(); ?>
                    </div>
                </div>
                <div class="back-image" style="background-image: url('<?php echo $enb_image; ?>')">
                    <div class="wrapper bottom">
                        <div class="wrap">
                            <div class="_12 centered">
                                <a href="#slide1" class="button"><?php echo $link_text; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php
        if (have_rows('ideas_repeater')): $slide = 1; ?>
            <?php while (have_rows('ideas_repeater')) : the_row();
                $db_image = get_sub_field('db_image');
                $mb_image = get_sub_field('mb_image');
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $wb = get_sub_field('wb_text');
                $section_link = get_sub_field('section_link'); ?>

                <section class="section slide js-slide" id="slide<?php echo $slide; ?>">
                    <div class="content-wrapper">
                    <?php if( isMobile() && $mb_image ){ ?>
                        <div id="image<?php echo $slide; ?>" class="back-image" style="background-image: url('<?php echo $mb_image; ?>')">
                    <?php } else { ?>
                        <div id="image<?php echo $slide; ?>" class="back-image" style="background-image: url('<?php echo $db_image; ?>')">
                    <?php } ?>
                            <div class="wrapper-fluid">
                                <div class="wrap">
                                    <div class="_8 _m6 title-wrap">
                                        <h4 class="title <?php echo $wb; ?>"><?php echo $title; ?></h4>
                                        <div class="text <?php echo $wb; ?>"><?php echo $text; ?></div>
                                    </div>
                                </div>
                                <?php if ($section_link): ?>
                                <div class="wrap bottom">
                                    <a class="button" href="<?php echo $section_link['url']; ?>" target="<?php echo $section_link['target']; ?>"><?php echo $section_link['title']; ?></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
                $slide++;
            endwhile; ?>
        <?php
        endif;
        if (have_rows('ideas_repeater')): $slide = 1; ?>
            <ul class="vertical-nav">
                <?php while (have_rows('ideas_repeater')) : the_row();
                    $navigation_title = get_sub_field('navigation_title'); ?>

                    <li class="nav-item">
                        <a href="#slide<?php echo $slide; ?>"><?php echo $navigation_title; ?></a>
                    </li>
                <?php
                $slide++;
                endwhile; ?>
            </ul>
        <?php
        endif;?>
        </main>
    </div>
<?php
get_footer();
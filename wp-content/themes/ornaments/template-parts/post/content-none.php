<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ornaments
 */

?>

<section class="section page-title">
    <div class="wrapper">
        <div class="wrap">
            <h1 class="_12 _m10 ofs_m1 title"><?php esc_html_e( 'Nothing Found', 'ornaments' ); ?></h1>
        </div>
    </div>
</section>
<section class="section no-results not-found">
    <div class="wrapper">
        <div class="wrap">

            <div class="_12 page-content">
                <?php
                if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

                    <p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'ornaments' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

                <?php elseif ( is_search() ) : ?>

                    <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'ornaments' ); ?></p>
                    <?php
                else : ?>

                    <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'ornaments' ); ?></p>
                    <?php
                endif; ?>
            </div><!-- .page-content -->
        </div>
    </div>
</section>

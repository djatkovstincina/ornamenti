<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ornaments
 */

?>
<a href="<?php echo get_permalink(); ?>" class="_12 link">
    <article id="post-<?php the_ID(); ?>" <?php post_class('_12'); ?>>
        <div class="date-title">
            <?php
            if ( 'post' === get_post_type() ) : ?>
                <div class="date">
                    <?php ornaments_posted_on() ; ?>
                </div>
            <?php
            endif;
            the_title( '<h2 class="entry-title">', '</h2>' );
            ?>
        </div>

        <div class="entry-summary">
            <?php
            $content = get_field('tcontent');
            the_ornaments_excerpt($content); ?>
        </div><!-- .entry-summary -->

    </article><!-- #post-## -->
</a>

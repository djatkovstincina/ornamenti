<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ornaments
 */

?>
<a href="<?php echo get_permalink(); ?>" class="_12 _s6 _m5 link">
    <article id="post-<?php the_ID(); ?>" class="post">
        <?php
        echo '<div class="blog-image border-img">';
        if( has_post_thumbnail() ):
            the_post_thumbnail('medium');
        else:
            echo '<div class="image" style="background-image:url(http://placehold.it/640x440)" alt="Featured Image"></div>';
        endif;
        echo '</div>';
        ?>
        <div class="date-title">
            <?php
            the_title( '<h2 class="entry-title">', '</h2>' );
            ?>
        </div>

        <div class="excerpt">
            <?php
            $content = get_field('tcontent');
            the_ornaments_excerpt($content); ?>

            <span class="read-more"><?php _e('Read More', 'ornaments'); ?></span>
        </div>
    </article>
</a>

<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ornaments
 */
?>
<a href="<?php echo get_permalink(); ?>" class="_12 _s6 _m4 link">
    <article id="post-<?php the_ID(); ?>" class="post">
        <div class="date-title">
            <?php
            the_title( '<h2 class="entry-title">', '</h2>' ); ?>
        </div>
        <?php
        echo '<div class="blog-image border-img">';
        if( has_post_thumbnail() ):
            the_post_thumbnail('large');
        else:
            echo '<div class="image" style="background-image:url(http://placehold.it/1400x500)" alt="Featured Image"></div>';
        endif;
            echo '<div class="text-wrap">';
                the_title( '<h5 class="title">', '</h5>' );
                echo '<div class="desc">' . mb_strimwidth(get_field('product_description'), 0, 70, '...') . '</div>';
            echo '</div>';
        echo '</div>';
        ?>
    </article>
</a>
<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ornaments
 */

?>
<article id="post-<?php the_ID(); ?>" class="_12 post">
    <div class="date-title">
        <?php
        the_title( '<h1 class="entry-title">', '</h1>' );
        if ( 'post' === get_post_type() ) : ?>
            <div class="date">
                <?php ornaments_posted_on() ; ?>
            </div>
        <?php
        endif;
        ?>
    </div>

    <div class="entry-content">
        <?php
        flex_post_content(); ?>
    </div>
    <div class="divider"></div>
    <div class="share-content">
        <span class="s-title"><?php _e('Share story:', 'ornaments'); ?></span>
        <?php
        echo get_share_link('facebook', true);
        echo get_share_link('twitter', true, $title);
        echo get_share_link('linkedin', true); ?>
    </div>
</article>

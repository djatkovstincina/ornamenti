<?php
/* Template Name: Setting Up */
get_header(); ?>
    <div id="content" class="site-content">
        <?php
        headerPage(); ?>
        <main id="main" class="page-main site-main" role="main">
            <?php
            the_breadcrumb(); ?>
                <section class="section setting-up">
                    <div class="wrapper">
                        <div class="wrap">
                            <?php
                            if ( have_rows('setting_up_repeater') ) :
                                $g_counter = 1;
                                while ( have_rows('setting_up_repeater') ) : the_row();
                                    $s_title = get_sub_field('setting_title');
                                    $s_gallery = get_sub_field('setting_gallery');
                                    $s_feature_i = get_sub_field('featured_image');
                                    $s_feature_v = get_sub_field('featured_video');
                                    if ($s_feature_v) {
                                        if (preg_match('/src="(.+?)"/', $s_feature_v, $matches)) {
                                            $src = $matches[1];
                                            $params = array(
                                                'controls' => 1,
                                                'hd' => 1,
                                                'fs' => 0,
                                                'rel' => 0,
                                                'modestbranding' => 0,
                                                'autoplay' => 0,
                                                'showinfo' => 0,
                                            );

                                            $new_src = add_query_arg($params, $src);
                                            $s_feature_v = str_replace($src, $new_src, $s_feature_v);
                                            $attributes = 'frameborder="0"';
                                            $s_feature_v = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $s_feature_v);
                                        }
                                    }

                                    if( $s_title ): ?>
                                    <div class="_12 s-title">
                                        <h2><?php echo $s_title; ?></h2>
                                    </div>
                                    <?php endif; ?>
                                    <div class="_12 s-feature">
                                        <?php
                                        if ($s_feature_i): ?>
                                            <a class="border-img i-link" data-fancybox="<?php echo $g_counter; ?>" href="<?php echo $s_feature_i; ?>">
                                                <img class="featured-image" src="<?php echo $s_feature_i; ?>" alt="Fetured Image" />
                                            </a>
                                        <?php
                                        elseif ($s_feature_v) : ?>
                                        <a class="i-link" data-fancybox="<?php echo $g_counter; ?>" href="<?php echo $src; ?>">
                                            <?php echo $s_feature_v; ?>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="_12">
                                        <div class="gallery-wrap">
                                            <?php
                                            if ($s_gallery):
                                                foreach( $s_gallery as $image ): ?>
                                                    <div class="border-img image">
                                                        <a class="i-link" data-fancybox="<?php echo $g_counter; ?>" href="<?php echo $image['url']; ?>">
                                                            <img src="<?php echo $image['url']; ?>" alt="Gallery Image" />
                                                        </a>
                                                    </div>
                                                <?php
                                                endforeach;
                                            endif;?>
                                        </div>
                                    </div>
                                <?php
                                $g_counter++;
                                endwhile;
                            endif; ?>
                        </div>
                    </div>
                </section>
            <?php
            if (have_rows('blocks_gallery')): ?>
                <section class="section gallery">
                    <div class="wrapper-fluid">
                        <?php while (have_rows('blocks_gallery')) : the_row();
                            $title = get_sub_field('title');
                            $text = get_sub_field('text'); ?>

                            <div class="wrap ing-wrap">
                            <div class="_12 _m4 title-wrap">
                                <h4 class="title"><?php echo $title; ?></h4>
                                <div class="text"><?php echo $text; ?></div>
                            </div>
                            <?php if (have_rows('images_gallery')): ?>
                                <div class="_12 _m8 images-wrap">
                                    <?php while (have_rows('images_gallery')) : the_row();
                                        $image = get_sub_field('image'); ?>
                                        <img class="gallery-image" src="<?php echo $image; ?>"/>
                                    <?php endwhile; ?>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </section>
            <?php
            endif;

            $section_title = get_field('section_title');
            if( have_rows('card_repeater') ): ?>
                <section class="section categories">
                    <div class="wrapper-fluid">
                        <div class="wrap">
                            <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
                        </div>
                        <div class="wrap">
                            <?php while ( have_rows('card_repeater') ) : the_row();
                                $card_image = get_sub_field('card_image');
                                $card_title = get_sub_field('card_title');
                                $card_link = get_sub_field('card_link'); ?>

                                <a href="<?php echo $card_link; ?>" class="link">
                                    <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                                        <div class="title-wrap">
                                            <h4 class="title"><?php echo $card_title; ?></h4>
                                        </div>
                                    </div>
                                    <span class='line-1'></span>
                                    <span class='line-2'></span>
                                    <span class='line-3'></span>
                                    <span class='line-4'></span>
                                </a>
                            <?php
                            endwhile; ?>
                        </div>
                    </div>
                </section>
            <?php
            endif; ?>
            <?php
            get_template_part('template-parts/layout/layout', 'newsletter');
            get_template_part('template-parts/layout/layout', 'links-banner');
            ?>
        </main>
    </div>
<?php
get_footer();
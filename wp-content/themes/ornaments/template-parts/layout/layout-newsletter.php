<?php
//          Start Newsletter section
$show_newsletter = get_field('dis_newsl');
$nl_t = get_field('nl_t', 'option');
$nl_st = get_field('nl_st', 'option');
$nsh = get_field('nsh', 'option');

if ($show_newsletter):
    ?>
    <section class="section newsletter">
        <div class="wrapper">
            <div class="wrap">
                <h2 class="title _xs12 _m8 ofs_m2"><?php if($nl_t) echo $nl_t; else echo 'Prijavite se na naš Newsletter!'; ?></h2>
                <p class="subtitle _xs12 _m8 ofs_m2"><?php if($nl_st) echo $nl_st; else echo 'Pridružite se... Nulla vel luctus purus. Nullam ut orci maximus, mollis arcu eget, maximus tortor. Curabitur non lorem...'; ?></p>
                <div class="form _m10 ofs_m1">
                    <?php if ($nsh):
                        echo do_shortcode($nsh);
                    else:
                        echo do_shortcode('[mc4wp_form id="310"]');
                    endif; ?>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php
endif;
?>
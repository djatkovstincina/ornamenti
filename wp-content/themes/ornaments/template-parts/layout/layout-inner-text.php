<?php
$tg = get_sub_field('t_g');
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$important_text = get_sub_field('important_text');
$tw = get_sub_field('t_w');
?>
<div class="wrap inner-text">
    <div class="_12  <?php if ($tg == 'white') echo 'white'; else echo 'gray'; ?>">
        <h1><?php echo $title; ?></h1>
        <h3><?php echo $subtitle; ?></h3>
        <div class="important"><?php echo $important_text; ?></div>
        <p><?php echo $tw; ?></p>
    </div>
</div>

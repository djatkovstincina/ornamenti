<?php
$thb_t_1 = get_sub_field('thb_t_1');
$thb_img_1 = get_sub_field('thb_img_1');
$thb_l_1 = get_sub_field('thb_l_1');
$thb_t_2 = get_sub_field('thb_t_2');
$thb_img_2 = get_sub_field('thb_img_2');
$thb_l_2 = get_sub_field('thb_l_2');
$thb_t_3 = get_sub_field('thb_t_3');
$thb_img_3 = get_sub_field('thb_img_3');
$thb_l_3 = get_sub_field('thb_l_3');
?>

<div class="wrap three-block animated">
    <a class="_m4 b-link" href="<?php echo $thb_l_1; ?>">
        <div class="item_c">
            <div class="item_c_lines">
                <div class="item_c_line-t"></div>
                <div class="item_c_line-r"></div>
                <div class="item_c_line-b"></div>
                <div class="item_c_line-l"></div>
            </div>
        </div>
        <div class="b-image" style="background-image: url('<?php echo $thb_img_1; ?>')">
            <div class="title-wrap">
                <h2 class="b-title"><?php echo $thb_t_1; ?></h2>
            </div>
        </div>
    </a>
    <a class="_m4 b-link" href="<?php echo $thb_l_2; ?>">
        <div class="item_c">
            <div class="item_c_lines">
                <div class="item_c_line-t"></div>
                <div class="item_c_line-r"></div>
                <div class="item_c_line-b"></div>
                <div class="item_c_line-l"></div>
            </div>
        </div>
        <div class="b-image" style="background-image: url('<?php echo $thb_img_2; ?>')">
            <div class="title-wrap">
                <h2 class="b-title"><?php echo $thb_t_2; ?></h2>
            </div>
        </div>
    </a>
    <a class="_m4 b-link" href="<?php echo $thb_l_3; ?>">
        <div class="item_c">
            <div class="item_c_lines">
                <div class="item_c_line-t"></div>
                <div class="item_c_line-r"></div>
                <div class="item_c_line-b"></div>
                <div class="item_c_line-l"></div>
            </div>
        </div>
        <div class="b-image" style="background-image: url('<?php echo $thb_img_3; ?>')">
            <div class="title-wrap">
                <h2 class="b-title"><?php echo $thb_t_3; ?></h2>
            </div>
        </div>
    </a>

</div>
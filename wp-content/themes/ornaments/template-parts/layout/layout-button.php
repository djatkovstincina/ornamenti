<?php
$bl = get_sub_field('b_l');
$bl_url = $bl['url'];
$bl_title = $bl['title'];
$bl_target = $bl['target'] ? $bl['target'] : '_self';
?>
<div class="wrap inner-button">
    <a class="inv-button" href="<?php echo $bl_url; ?>" target="<?php echo $bl_target; ?>"><?php echo $bl_title; ?></a>
</div>

<?php
$tb_t_1 = get_sub_field('tb_t_1');
$tb_img_1 = get_sub_field('tb_img_1');
$tb_l_1 = get_sub_field('tb_l_1');
$tb_t_2 = get_sub_field('tb_t_2');
$tb_img_2 = get_sub_field('tb_img_2');
$tb_l_2 = get_sub_field('tb_l_2');
?>

<div class="wrap two-block animated">
    <a class="_m6 b-link" href="<?php echo $tb_l_1; ?>">
        <div class="item_c">
            <div class="item_c_lines">
                <div class="item_c_line-t"></div>
                <div class="item_c_line-r"></div>
                <div class="item_c_line-b"></div>
                <div class="item_c_line-l"></div>
            </div>
        </div>
        <div class="b-image" style="background-image: url('<?php echo $tb_img_1; ?>')">
            <div class="title-wrap">
                <h2 class="b-title"><?php echo $tb_t_1; ?></h2>
            </div>
        </div>
    </a>
    <a class="_m6 b-link" href="<?php echo $tb_l_2; ?>">
        <div class="item_c">
            <div class="item_c_lines">
                <div class="item_c_line-t"></div>
                <div class="item_c_line-r"></div>
                <div class="item_c_line-b"></div>
                <div class="item_c_line-l"></div>
            </div>
        </div>
        <div class="b-image" style="background-image: url('<?php echo $tb_img_2; ?>')">
            <div class="title-wrap">
                <h2 class="b-title"><?php echo $tb_t_2; ?></h2>
            </div>
        </div>
    </a>

</div>
<?php if (have_rows('th_r')): ?>
    <div class="wrap three-blocks">
        <?php while (have_rows('th_r')) : the_row();
            $thih1 = get_sub_field('thih1');
            $thid1 = get_sub_field('th_i_d1');
            $thih2 = get_sub_field('thih2');
            $thid2 = get_sub_field('th_i_d2');
            $thih3 = get_sub_field('thih3');
            $thid3 = get_sub_field('th_i_d3');
            $id = get_the_ID(); ?>
            <div class="_12 _m4 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $thih1; ?>">
                    <img class="h-image" src="<?php echo $thih1; ?>">
                </a>
                <?php if($thid1): ?>
                    <div class="description"><?php echo $thid1; ?></div>
                <?php endif; ?>
            </div>
            <div class="_12 _m4 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $thih2; ?>">
                    <img class="h-image" src="<?php echo $thih2; ?>">
                </a>
                <?php if($thid2): ?>
                    <div class="description"><?php echo $thid2; ?></div>
                <?php endif; ?>
            </div>
            <div class="_12 _m4 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $thih3; ?>">
                    <img class="h-image" src="<?php echo $thih3; ?>">
                </a>
                <?php if($thid3): ?>
                    <div class="description"><?php echo $thid3; ?></div>
                <?php endif; ?>
            </div>
        <?php
        endwhile; ?>
    </div>
<?php endif; ?>

<?php if (have_rows('f_r')): ?>
    <div class="wrap four-blocks">
        <?php while (have_rows('f_r')) : the_row();
            $fih1 = get_sub_field('fih1');
            $fid1 = get_sub_field('f_i_d1');
            $fih2 = get_sub_field('fih2');
            $fid2 = get_sub_field('f_i_d2');
            $fih3 = get_sub_field('fih3');
            $fid3 = get_sub_field('f_i_d3');
            $fih4 = get_sub_field('fih4');
            $fid4 = get_sub_field('f_i_d4');
            $id = get_the_ID(); ?>
            <div class="_12 _m6 _l3 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $fih1; ?>">
                    <img class="h-image" src="<?php echo $fih1; ?>">
                </a>
                <?php if($fid1): ?>
                    <div class="description"><?php echo $fid1; ?></div>
                <?php endif; ?>
            </div>
            <div class="_12 _m6 _l3 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $fih2; ?>">
                    <img class="h-image" src="<?php echo $fih2; ?>">
                </a>
                <?php if($fid2): ?>
                    <div class="description"><?php echo $fid2; ?></div>
                <?php endif; ?>
            </div>
            <div class="_12 _m6 _l3 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $fih3; ?>">
                    <img class="h-image" src="<?php echo $fih3; ?>">
                </a>
                <?php if($fid3): ?>
                    <div class="description"><?php echo $fid3; ?></div>
                <?php endif; ?>
            </div>
            <div class="_12 _m6 _l3 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $fih4; ?>">
                    <img class="h-image" src="<?php echo $fih4; ?>">
                </a>
                <?php if($fid4): ?>
                    <div class="description"><?php echo $fid4; ?></div>
                <?php endif; ?>
            </div>
        <?php
        endwhile; ?>
    </div>
<?php endif; ?>

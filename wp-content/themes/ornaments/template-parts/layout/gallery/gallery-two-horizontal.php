<?php if (have_rows('t_r')): ?>
    <div class="wrap two-blocks">
        <?php while (have_rows('t_r')) : the_row();
            $tih1 = get_sub_field('tih1');
            $tid1 = get_sub_field('t_i_d1');
            $tih2 = get_sub_field('tih2');
            $tid2 = get_sub_field('t_i_d2');
            $id = get_the_ID(); ?>
            <div class="_12 _m6 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $tih1; ?>">
                    <img class="h-image" src="<?php echo $tih1; ?>">
                </a>
                <?php if($tid1): ?>
                    <div class="description"><?php echo $tid1; ?></div>
                <?php endif; ?>
            </div>
            <div class="_12 _m6 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $tih2; ?>">
                    <img class="h-image" src="<?php echo $tih2; ?>">
                </a>
                <?php if($tid2): ?>
                    <div class="description"><?php echo $tid2; ?></div>
                <?php endif; ?>
            </div>
        <?php
        endwhile; ?>
    </div>
<?php endif; ?>

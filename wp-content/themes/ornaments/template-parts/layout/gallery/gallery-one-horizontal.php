<?php if (have_rows('o_r')): ?>
    <div class="wrap one-block">
        <?php while (have_rows('o_r')) : the_row();
            $oih = get_sub_field('oih');
            $o_i_d = get_sub_field('o_i_d');
            $id = get_the_ID(); ?>
            <div class="_12 _m10 ofs_m1 _xl8 ofs_xl2 h-image-wrap">
                <a class="i-link border-img" data-fancybox="gallery<?php echo $id; ?>" href="<?php echo $oih; ?>">
                    <img class="h-image" src="<?php echo $oih; ?>">
                </a>
                <?php if($o_i_d): ?>
                <div class="description"><?php echo $o_i_d; ?></div>
                <?php endif; ?>
            </div>
        <?php
        endwhile; ?>
    </div>
<?php endif; ?>
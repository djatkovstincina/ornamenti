<?php
$section_title = get_field('section_title', get_option('page_for_posts'));
if( have_rows('card_repeater', get_option('page_for_posts')) ): ?>
<section class="section categories">
    <div class="wrapper-fluid">
        <div class="wrap">
            <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
        </div>
        <div class="wrap">
            <?php
                while ( have_rows('card_repeater', get_option('page_for_posts')) ) : the_row();
                    $card_image = get_sub_field('card_image', get_option('page_for_posts'));
                    $card_title = get_sub_field('card_title', get_option('page_for_posts'));
                    $card_link = get_sub_field('card_link', get_option('page_for_posts')); ?>

                    <a href="<?php echo $card_link; ?>" class="link">
                        <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                            <div class="title-wrap">
                                <h2 class="title"><?php echo $card_title; ?></h2>
                            </div>
                        </div>
                        <span class='line-1'></span>
                        <span class='line-2'></span>
                        <span class='line-3'></span>
                        <span class='line-4'></span>
                    </a>
                <?php
                endwhile; ?>
        </div>
    </div>
</section>
<?php
endif; ?>
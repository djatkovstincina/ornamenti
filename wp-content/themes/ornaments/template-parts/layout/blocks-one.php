<?php
$ob_t = get_sub_field('ob_t');
$ob_img = get_sub_field('ob_img');
$ob_l = get_sub_field('ob_l');
?>

<div class="wrap one-block animated">
    <a class="_12 b-link" href="<?php echo $ob_l; ?>">
        <div class="item_c">
            <div class="item_c_lines">
                <div class="item_c_line-t"></div>
                <div class="item_c_line-r"></div>
                <div class="item_c_line-b"></div>
                <div class="item_c_line-l"></div>
            </div>
        </div>
        <div class="b-image" style="background-image: url('<?php echo $ob_img; ?>')">
            <div class="title-wrap">
                <h2 class="b-title"><?php echo $ob_t; ?></h2>
            </div>
        </div>
    </a>
</div>
<?php

if (have_rows('l_r')): ?>
    <?php while (have_rows('l_r')) : the_row();
        $style = get_sub_field('choose_background');
        $lr_i = get_sub_field('lr_i');
        $lr_t = get_sub_field('lr_t'); ?>
        <div class="wrap left-right">
            <div class="_12 _m6 lr-image-wrap">
                <a class="i-link border-img" href="<?php echo $lr_i; ?>" data-fancybox>
                    <img class="image" src="<?php echo $lr_i; ?>">
                </a>
            </div>
            <div class="_12 _m6 lr-text-wrap <?php if ($style == 'style1') echo 'style blue'; elseif ($style == 'style2') echo 'style green'; elseif ($style == 'style3') echo 'style orange'; elseif ($style == 'style4') echo 'style red'; elseif ($style == 'style5') echo 'style gray'; else echo ''; ?>">
                <div class="description"><?php echo $lr_t; ?></div>
            </div>
        </div>
    <?php endwhile;
endif; ?>

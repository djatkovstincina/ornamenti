<?php
$v_control = get_sub_field('v_c');
$poster = get_sub_field('poster');
$i_video_mp4 = get_sub_field('v_mp4');
$i_video_webm = get_sub_field('v_webm');
$i_video_ogv = get_sub_field('v_ogv');
$embed = get_sub_field('e_v');
if ($embed) {
    if (preg_match('/src="(.+?)"/', $embed, $matches)) {
        $src = $matches[1];
        $params = array(
            'controls' => 1,
            'hd' => 1,
            'fs' => 0,
            'rel' => 0,
            'modestbranding' => 0,
            'autoplay' => 0,
            'showinfo' => 0,
        );

        $new_src = add_query_arg($params, $src);
        $embed = str_replace($src, $new_src, $embed);
        $attributes = 'frameborder="0"';
        $embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $embed);
    }
}?>

<div class="wrap">
    <?php
    if ( $v_control ) : ?>
        <div class="_12 _m10 ofs_m1 video-container">
            <video id="inner-video" class="js-video inner-video" loop data-object="video"<?php if ($poster) echo 'poster="' . $poster . '"'; ?> controls playsinline controlsList="nodownload">
                <?php
                if( $i_video_mp4 )echo '<source src="' . $i_video_mp4 .'" type="video/mp4">';
                if( $i_video_webm ) echo '<source src="' . $i_video_webm .'" type="video/webm">';
                if( $i_video_ogv ) echo '<source src="' . $i_video_ogv .'" type="video/ogg">'; ?>
            </video>
        </div>
    <?php
    else: ?>
        <div class="_12 _m10 ofs_m1 embed-video">
            <?php echo $embed; ?>
        </div>
    <?php endif; ?>
</div>

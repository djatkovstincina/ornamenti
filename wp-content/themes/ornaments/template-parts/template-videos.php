<?php
/*
    Template Name: Video Gallery
*/
get_header();
$left_title = get_field('left_title');
$left_text = get_field('left_text');
$fv_image = get_field('fv_image');
$fv_choise = get_field('fv_choise');
$f_mp = get_field('mp4_fv');
$f_webm = get_field('webm_fv');
$f_ogv = get_field('ogg_fv');
$f_embed = get_field('embed_fv');
if ($f_embed) {
    preg_match('/src="(.+?)"/', $f_embed, $matches);
    $src = $matches[1];
    $params = array(
        'controls' => 1,
        'hd' => 1,
        'rel' => 0,
        'autohide' => 1,
        'autoplay' => 0,
        'showinfo' => 0,
        'height' => '360px',
        'width' => '640px'
    );
    $new_src = add_query_arg($params, $src);
    $f_embed = str_replace($src, $new_src, $f_embed);
    $attributes = 'frameborder="0"';
    $f_embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $f_embed);
}
$right_title = get_field('right_title');
$right_text = get_field('right_text');
$right_button = get_field('right_button');
?>
<div id="content" class="site-content">
    <?php
    headerPage(); ?>
    <main id="main" class="page-main site-main" role="main">
        <?php
        the_breadcrumb(); ?>
        <section class="section video">
            <div class="wrapper">
                <div class="wrap">

                    <div class="_12 _m4 single-text">
                        <h3 class="title"><?php echo $left_title; ?></h3>
                        <div class="text"><?php echo $left_text; ?></div>
                    </div>
                    <div class="_12 _m4">
                        <div class="featured-video js-video">
                            <?php if ($fv_choise == 'Embedded link'): ?>
                                <a href="<?php echo $new_src; ?>" data-fancybox title="Featured video" class="poster">
                                    <div class="hover-box">
                                        <span class="hover-link"><i class="fa fa-play"></i></span>
                                    </div>
                                    <?php echo $f_embed; ?>
                                </a>
                            <?php elseif ($fv_choise == 'Media Library link'): ?>
                                <a class="poster no-scroll" href="<?php echo $f_mp; ?>" data-fancybox title="Featured video">
                                    <div class="hover-box">
                                        <span class="hover-link"><i class="fa fa-play"></i></span>
                                    </div>
                                    <video id="f-video" class="js-video" <?php if ($fv_image) echo 'poster="' . $fv_image . '"'; ?> preload loop controls controlsList="nodownload">
                                        <?php
                                        echo '<source src="' . $f_mp . '" type="video/mp4">';
                                        if ($f_webm) echo '<source src="' . $f_webm . '" type="video/webm">';
                                        if ($f_ogv) echo '<source src="' . $f_ogv . '" type="video/ogg">'; ?>
                                    </video>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="_12 _m4 single-text">
                        <h3 class="title"><?php echo $right_title; ?></h3>
                        <div class="text"><?php echo $right_text; ?></div>
                        <a class="button" href="<?php echo $right_button['url']; ?>" target="<?php echo $right_button['target']; ?>"><?php echo $right_button['title']; ?></a>
                    </div>

                    <?php
                    if (have_rows('videos')):
                        $num = 1; ?>
                    <div class="wrapper">
                        <div class="video-gallery wrap">
                        <?php
                        while (have_rows('videos')): the_row();
                            $v_image = get_sub_field('v_image');
                            $v_title = get_sub_field('v_title');
                            $v_desc = get_sub_field('v_descr');
                            $v_choise = get_sub_field('v_choise');
                            $mp = get_sub_field('mp4_video');
                            $webm = get_sub_field('webm_video');
                            $ogv = get_sub_field('ogg_video');
                            $embed = get_sub_field('embed');
                            if ($embed) {
                                preg_match('/src="(.+?)"/', $embed, $matches);
                                $src = $matches[1];
                                $params = array(
                                    'controls' => 1,
                                    'hd' => 1,
                                    'rel' => 0,
                                    'autohide' => 1,
                                    'autoplay' => 0,
                                    'showinfo' => 0,
                                    'height' => '360px',
                                    'width' => '640px'
                                );
                                $new_src = add_query_arg($params, $src);
                                $embed = str_replace($src, $new_src, $embed);
                                $attributes = 'frameborder="0"';
                                $embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $embed);
                            } ?>
                            <div class="_12 _m4 single-video">
                                <h3 class="video-title">
                                    <?php echo $v_title; ?>
                                </h3>
                                <div class="video-image js-video">
                                    <?php if ($v_choise == 'Embedded link'): ?>
                                        <a href="<?php echo $new_src; ?>" data-fancybox title="<?php echo $v_title ?>" class="poster">
                                            <div class="hover-box">
                                                <span class="hover-link"><i class="fa fa-play"></i></span>
                                            </div>
                                            <?php echo $embed; ?>
                                        </a>
                                    <?php elseif ($v_choise == 'Media Library link'): ?>
                                        <div class="poster no-scroll" href="<?php echo $mp; ?>" data-fancybox title="<?php echo $v_title ?>">
                                            <div class="hover-box">
                                                <span class="hover-link"><i class="fa fa-play"></i></span>
                                            </div>
                                            <video id="i-video-<?php echo $num; ?>" <?php if ($v_image) echo 'poster="' . $v_image . '"'; ?> preload controls controlsList="nodownload">
                                                <?php
                                                echo '<source src="' . $mp . '" type="video/mp4">';
                                                if ($webm) echo '<source src="' . $webm . '" type="video/webm">';
                                                if ($ogv) echo '<source src="' . $ogv . '" type="video/ogg">'; ?>
                                            </video>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="video-desc"><?php echo $v_desc; ?></div>
                            </div>
                            <?php $num++; ?>
                        <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <?php
        if (have_rows('blocks_gallery')): ?>
            <section class="section gallery">
                <div class="wrapper-fluid">
                    <?php while (have_rows('blocks_gallery')) : the_row();
                        $title = get_sub_field('title');
                        $text = get_sub_field('text'); ?>

                        <div class="wrap ing-wrap">
                        <div class="_12 _m4 title-wrap">
                            <h4 class="title"><?php echo $title; ?></h4>
                            <div class="text"><?php echo $text; ?></div>
                        </div>
                        <?php if (have_rows('images_gallery')): ?>
                            <div class="_12 _m8 images-wrap">
                                <?php while (have_rows('images_gallery')) : the_row();
                                    $image = get_sub_field('image'); ?>
                                    <img class="gallery-image" src="<?php echo $image; ?>"/>
                                <?php endwhile; ?>
                            </div>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </section>
        <?php
        endif;

        $section_title = get_field('section_title');
        if( have_rows('card_repeater') ): ?>
            <section class="section categories">
                <div class="wrapper-fluid">
                    <div class="wrap">
                        <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
                    </div>
                    <div class="wrap">
                        <?php while ( have_rows('card_repeater') ) : the_row();
                            $card_image = get_sub_field('card_image');
                            $card_title = get_sub_field('card_title');
                            $card_link = get_sub_field('card_link'); ?>

                            <a href="<?php echo $card_link; ?>" class="link">
                                <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                                    <div class="title-wrap">
                                        <h4 class="title"><?php echo $card_title; ?></h4>
                                    </div>
                                </div>
                                <span class='line-1'></span>
                                <span class='line-2'></span>
                                <span class='line-3'></span>
                                <span class='line-4'></span>
                            </a>
                        <?php
                        endwhile; ?>
                    </div>
                </div>
            </section>
        <?php
        endif; ?>
    </main>
</div>

<?php get_footer(); ?>

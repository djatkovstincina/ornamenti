<?php
/**
 * Header for pages
 * Custom header image functionality
 * @package WordPress
 */

/**
 * @return mixed|string in case there is a image array in field, returns random image from it, otherwise returns random.jpg from images
 *
 */

/**
 * Custom header for Aquaplan home page
 * Change name of images that show up by default if nothing is selected (from random.jpg)
 */
function randomHeaderImage() {

    $images = array();

    if( have_rows('imgs', 'options') ):
        while( have_rows('imgs', 'options')): the_row();
            $image = get_sub_field('img');

            array_push($images, $image);
        endwhile;
        $random_counter = rand(0, count($images)-1);

    endif;
    if( !empty($images) ) return $images[$random_counter];
    else return get_template_directory_uri() . '/src/images/random.jpg';
}

function ornamentsHomeHeader() {
    $header_height = get_field('h_height');
    $random_image = get_template_directory_uri() . '/src/images/random.jpg';
    $header_control = get_field('h_control');
    $header_external = get_field('h_external');
    $header_video_mp4 = get_field('h_mp4');
    $header_video_webm = get_field('h_webm');
    $header_video_ogv = get_field('h_ogv');
    $embed = get_field('external_video');
    if ( $embed ) {
        preg_match('/src="(.+?)"/', $embed, $matches);
        $src = $matches[1];
        $params = array(
            'controls'    => 1,
            'hd'        => 1,
            'rel' => 0,
            'autohide'    => 1,
            'autoplay' => 0,
            'showinfo' => 0,
        );
        $new_src = add_query_arg($params, $src);
        $embed = str_replace($src, $new_src, $embed);
        $attributes = 'frameborder="0"';
        $embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $embed);
    } ?>

    <section id="header-section" class="header-section<?php if ( !$header_control ) echo ' wrapper-fluid'; else echo ' wrapper'; if ( $header_height === 'Full Screen' ) echo ' full'; ?>">
        <?php
        if ( $header_control ) :
            if ( $header_external ) : ?>
            <div class="wrap video-container -external">
                <?php echo $embed; ?>
            </div>
            <?php
            else: ?>
            <div class="wrap video-container">
                <video id="header-video" data-object="video" class="site-header-video" poster="<?php echo $random_image; ?>" style="background-image: url(<?php echo $background_image; ?>);" autoplay playsinline muted loop>
                    <?php
                    if ( $header_video_ogv ) : ?>
                        <source src="<?php echo $header_video_ogv; ?>" type="video/ogv">
                    <?php
                    endif;
                    if ( $header_video_webm ) : ?>
                        <source src="<?php echo $header_video_webm; ?>" type="video/webm">
                    <?php
                    endif; ?>
                    <source src="<?php echo $header_video_mp4; ?>" type="video/mp4">
                </video>
            </div>
        <?php
            endif;
        elseif ( have_rows('h_slider') ): ?>
            <div class="wrap header-slider border-img">
                <?php while ( have_rows('h_slider') ) : the_row();
                    $header_image = get_sub_field('h_image');
                    $black = get_sub_field('bb');
                    $header_title = get_sub_field('h_title');
                    $header_subtitle = get_sub_field('h_subtitle');
                    $header_link = get_sub_field('h_link');
                    $link_type = get_sub_field('l_type');
                    $link_label = get_sub_field('l_label');
                    $link_internal = get_sub_field('l_ilink');
                    $link_external = get_sub_field('l_elink'); ?>
                    <div class="header-slide" data-image="<?php echo $header_image; ?>" style="background-image: url(<?php echo $header_image; ?>);">
                        <div class="inner-image animated-image" style="background-image: url(<?php echo $header_image; ?>);"></div>
                        <div class="wrapper">
                            <div class="header-info home <?php if($black) echo 'black'; ?>">
                                <?php
                                if ( $header_title ) : ?>
                                    <h1 class="header-title">
                                        <?php
                                        echo $header_title; ?>
                                    </h1>
                                <?php
                                endif;
                                if ( $header_subtitle ) : ?>
                                    <p class="header-subtitle">
                                        <?php
                                        echo $header_subtitle; ?>
                                    </p>
                                <?php
                                endif; ?>
                            </div>
                            <?php
                            if ( $header_link === 'Link' ) : ?>
                                <div class="header-link home">
                                    <?php
                                    if ( $link_type === 'Internal' ) : ?>
                                        <a href="<?php echo $link_internal; ?>" class="link">
                                            <span><?php echo $link_label; ?></span>
                                            <i class="fa fa-chevron-right"></i>
                                        </a>
                                    <?php
                                    else : ?>
                                        <a href="<?php echo $link_external; ?>" class="link" target="_blank">
                                            <span><?php echo $link_label; ?></span>
                                            <i class="fa fa-chevron-right"></i>
                                        </a>
                                    <?php
                                    endif; ?>
                                </div>
                            <?php
                            endif; ?>
                        </div>
                    </div>
                <?php
                endwhile; ?>
            </div>
        <?php
        else : ?>
            <div class="wrap header-image" style="background-image: url(<?php echo $random_image; ?>);"></div>
        <?php
        endif;
        ?>
    </section>
    <div class="section-social">
        <div class="header-social">
            <?php echo ornaments_social_network(); ?>
        </div>
    </div>
    <?php
}

/**
 * Custom header for all other pages
 * Change name of images that show up by default if nothing is selected (from random.jpg)
 */
function headerPage() {
    $header_height = get_field('h_height');
    $random_image = get_template_directory_uri() . '/src/images/random.jpg';
    $header_control = get_field('h_control');
    $header_video_mp4 = get_field('h_mp4');
    $header_video_webm = get_field('h_webm');
    $header_video_ogv = get_field('h_ogv');
    $header_image_num = get_field('h_slider' );
    $header_external = get_field('h_external');
    $embed = get_field('external_video');
    if ( $embed ) {
        preg_match('/src="(.+?)"/', $embed, $matches);
        $src = $matches[1];
        $params = array(
            'controls'    => 1,
            'hd'        => 1,
            'rel' => 0,
            'autohide'    => 1,
            'autoplay' => 0,
            'showinfo' => 0,
        );
        $new_src = add_query_arg($params, $src);
        $embed = str_replace($src, $new_src, $embed);
        $attributes = 'frameborder="0"';
        $embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $embed);
    }
    $count = $header_image_num; ?>

    <section id="header-section" class="header-section<?php if ( !$header_control ) echo ' wrapper-fluid'; else echo ' wrapper'; if ( $header_height === 'Full Screen' ) echo ' full'; ?>">
        <?php
        if ( $header_control ) :
            if ( $header_external ) : ?>
                <div class="wrap video-container -external">
                    <?php echo $embed; ?>
                </div>
            <?php
            else: ?>
                <div class="wrap video-container">
                    <video id="header-video" data-object="video" class="site-header-video" poster="<?php echo $random_image; ?>" autoplay playsinline muted loop>
                        <?php
                        if ( $header_video_ogv ) : ?>
                            <source src="<?php echo $header_video_ogv; ?>" type="video/ogv">
                        <?php
                        endif;
                        if ( $header_video_webm ) : ?>
                            <source src="<?php echo $header_video_webm; ?>" type="video/webm">
                        <?php
                        endif; ?>
                        <source src="<?php echo $header_video_mp4; ?>" type="video/mp4">
                    </video>
                </div>
            <?php
            endif;
        elseif ( have_rows('h_slider') ): ?>
            <div class="wrap header-slider border-img">
                <?php while ( have_rows('h_slider') ) : the_row();
                    $header_image = get_sub_field('h_image');
                    $black = get_sub_field('bb');
                    $header_title = get_sub_field('h_title');
                    $header_subtitle = get_sub_field('h_subtitle'); ?>
                    <div class="header-slide <?php if($count==1) echo 'inner'; ?>" data-image="<?php echo $header_image; ?>" style="background-image: url(<?php echo $header_image; ?>);">
                        <div class="inner-image animated-image" style="background-image: url(<?php echo $header_image; ?>);"></div>

                        <div class="wrapper">
                            <div class="header-info _12 <?php if($black) echo 'black'; ?>">
                                <?php
                                if ( $header_title ) : ?>
                                    <h1 class="header-title">
                                        <?php
                                        echo $header_title; ?>
                                    </h1>
                                <?php
                                endif;
                                if ( $header_subtitle ) : ?>
                                    <p class="header-subtitle">
                                        <?php
                                        echo $header_subtitle; ?>
                                    </p>
                                <?php
                                endif; ?>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile; ?>
            </div>
        <?php
        elseif( has_post_thumbnail() ): ?>
            <div class="wrap header-slider">
                <div class="header-slide" data-image="<?php the_post_thumbnail_url('large'); ?>" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>);">
                    <div class="inner-image animated-image" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>);"></div>
                </div>
            </div>
        <?php
        else : ?>
            <div class="wrap header-image" style="background-image: url(<?php echo $random_image; ?>);"></div>
        <?php
        endif;
        ?>
    </section>
    <div class="section-social">
        <div class="header-social">
            <?php echo ornaments_social_network(); ?>
        </div>
    </div>
    <?php
}

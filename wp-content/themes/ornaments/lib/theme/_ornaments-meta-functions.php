<?php
/**
 * Custom meta functions for ornaments
 * Functions like custom excerpt, share link, mail chimp integration etc...
 */

/**
 * Custom Excerpt function for Advanced Custom Fields
 * @param $field - ACF field with content to do excerpt
 */
function get_ornaments_excerpt($field, $count = '', $end = '') {;

    if ( '' != $field ) {
        $field = strip_shortcodes( $field );
        $field = apply_filters('the_content', $field);
        $field = str_replace(']]&gt;', ']]&gt;', $field);
        //$excerpt_length = rand(22, 60); for randoming
        if( $count ) $excerpt_length = $count;
        else $excerpt_length = 60;

        if( $end ) $excerpt_more = apply_filters('excerpt_more', ' ' . $end);
        else $excerpt_more = apply_filters('excerpt_more', ' ' . '...');

        $field = wp_trim_words( $field, $excerpt_length, $excerpt_more );
    }
    return apply_filters('the_excerpt', $field);
}

/**
 * Custom excerpt function for Advanced Custom Fields
 *
 * Echo Custom Excerpt function for Advanced Custom Fields
 * @param $field - ACF field with content to do excerpt
 */
function the_ornaments_excerpt($field, $count = '', $end = '') {
    echo get_ornaments_excerpt($field, $count, $end);
}

/**
 * Get the sharing link
 *
 * Helper function for share links
 *
 * @param  string $network   Default '', network to share to, values to use: twitter, facebook, google, linkedin
 * @param  boolean $icon     Default false, set to true, if you want icon instead of text
 * @param  string $hashtags  Hashatgs (Twitter)
 * @param  string $text      Some networks allow extra text (Twitter)
 * @return string            Returns share link markup
 */
function    get_share_link($network = '', $icon = false, $text = '', $hashtags = '') {
    $link = get_permalink();
    if( !$text ) $text = get_the_title();
    if ( $network == 'twitter' ) {
        /**
         * Example usage: share_link('facebook', true); or share_link('twitter', false, 'lol,hi,hashtag', 'Custom Text For Twitter') ...
         */
        $href = 'http://twitter.com/share?text=' . $text . ' - ' . '&url=' . $link . '&hashtags=' . $hashtags;
        if( !$icon ) $print_name = 'Twitter'; else $print_name = '<i class="fab fa-twitter" aria-hidden="true"></i>';

    } elseif ( $network == 'facebook' ) {
        $href = 'http://www.facebook.com/sharer/sharer.php?u=' . $link;
        if( !$icon ) $print_name = 'Facebook'; else $print_name = '<i class="fab fa-facebook" aria-hidden="true"></i>';
    } elseif ( $network == 'google' ) {
        $href = 'https://plus.google.com/share?url=' . $link;
        if( !$icon ) $print_name = 'Google +'; else $print_name = '<i class="fab fa-google-plus" aria-hidden="true"></i>';
    } elseif ( $network == 'linkedin' ) {
        $href = 'https://www.linkedin.com/shareArticle?mini=true&url=' . $link . '&title=' . $text . '&summary=&source=';
        if( !$icon ) $print_name = 'Linkedin'; else $print_name = '<i class="fab fa-linkedin" aria-hidden="true"></i>';
    } else {
        return;
    }

    $output = '<a href="' . $href . '"';
    $output .= ' onclick="javascript:window.open(this.href, \'\',\'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=650,centerscreen=yes\');return false;"';
    $output .= '>';
    $output .= $print_name;
    $output .= '</a>';

    return $output;
}

/**
 * Share link
 *
 * Echo share link
 *
 * @param  string $network   Default '', network to share to, values to use: twitter, facebook, google, linkedin
 * @param  boolean $icon     Default false, set to true, if you want icon instead of text
 * @param  string $hashtags  Hashatgs (Twitter)
 * @param  string $text      Some networks allow extra text (Twitter)
 * @return string            Echo share link markup
 */
function the_share_link($network = '', $icon = '', $text = '', $hashtags = '') {
    echo get_share_link($network, $icon, $text, $hashtags);
}

/**
 * Get first item from array
 * @param $vars {array} - List of variables
 * @return mixed - First available in array
 */
function get_ornaments_first($vars) {
    if ( !$vars )
        return '';

    foreach ( $vars as $var ) :
        if ( $var )
            return $var;
    endforeach;
}

/**
 * Echo first item from array
 * @param $vars {array} - List of variables
 * @return mixed - First available in array
 */
function the_ornaments_first($vars) {
    if ( !$vars )
        return '';

    foreach ( $vars as $var ) :
        if ( $var ) {
            echo $var;
            return;
        }
    endforeach;
}


//Custom Post navigation
function ornaments_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /**	Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /**	Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="_12 navigation archive-navigation"><ul>' . "\n";

    if ( get_previous_posts_link('<<') )
        printf( '<li class="first">%s</li>' . "\n", get_previous_posts_link('<i class="fa fa-chevron-left" aria-hidden="true"></i>') );

    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    if ( get_next_posts_link('>>') )
        printf( '<li class="last">%s</li>' . "\n", get_next_posts_link('<i class="fa fa-chevron-right" aria-hidden="true"></i>') );

    echo '</ul></div>' . "\n";
}

/**
 * Custom number of posts per page on blog
 */

add_action('pre_get_posts','ppp_and_offset_category_page');
function ppp_and_offset_category_page( $query ) {
    if ($query->is_home() && $query->is_main_query() && !is_admin()) {
        $ppp = get_option('posts_per_page');
        $offset = 1;
        if (!$query->is_paged()) {
            $query->set('posts_per_page',$offset + $ppp);
        } else {
            $offset = $offset + ( ($query->query_vars['paged']-1) * $ppp );
            $query->set('posts_per_page',$ppp);
            $query->set('offset',$offset);
        }
    }
}

add_filter( 'found_posts', 'category_offset_pagination', 10, 2 );
function category_offset_pagination( $found_posts, $query ) {
    $offset = 1;

    if( !is_admin() && $query->is_home() && $query->is_main_query() ) {
        $found_posts = $found_posts - $offset;
    }
    return $found_posts;
}

/**
 * List of CPT in Contact Form 7 dropdown
 *
 * @param $tag
 * @param $unused
 * @return mixed
 */

function dynamic_field_values ( $tag, $unused ) {

    if ( $tag['name'] != 'product-list' )
        return $tag;

    $args = array (
        'numberposts'   => -1,
        'post_type'     => 'product',
        'orderby'       => 'title',
        'order'         => 'ASC',
    );

    $custom_posts = get_posts($args);

    if ( ! $custom_posts )
        return $tag;

    foreach ( $custom_posts as $custom_post ) {

        $tag['raw_values'][] = $custom_post->post_title;
        $tag['values'][] = $custom_post->post_title;
        $tag['labels'][] = $custom_post->post_title;

    }

    return $tag;

}

add_filter( 'wpcf7_form_tag', 'dynamic_field_values', 10, 2);

<?php
/**
 * @package ornaments
 */

$footer_banners = get_field('footer_banners', 'option');
$footer_links = get_field('footer_links', 'option');
$footer_logo = get_field('footer_logo', 'option');
$footer_text = get_field('footer_text', 'option');
$contact_info = get_field('contact_info', 'option');
$copyright_text = get_field('copyright_text', 'option');
?>
<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="wrapper-fluid top-footer">
        <a id="toTop" class="toTop"><i class="fa fa-chevron-up"></i></a>
        <img class="shipping" src="<?php echo get_template_directory_uri() . '/bundles/images/_dep/shipping-worldwide.png'; ?>"/>
        <div class="wrap">
            <?php
            if($footer_banners) :
                foreach ($footer_banners as $page):
                $page_title = get_the_title($page['links']);
                $page_link = get_the_permalink($page['links']);
                $page_img = $page['page_image']; ?>
                    <a class="_12 _s6 banner" href="<?php echo $page_link; ?>" style="background-image: url('<?php echo $page_img; ?>')">
                        <div class="banner-overlay"></div>
                        <div class="banner-text">
                            <h3 class="banner-title"><?php echo $page_title; ?></h3>
                            <p class="see-more">View page<i class="fa fa-chevron-right"></i></p>
                        </div>
                    </a>
            <?php
                endforeach;
            endif;?>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="lines"></div>
        <div class="wrapper">
            <div class="wrap">
                <div class="_12 _s3 ord_2 ord_s1 part  -center">
                    <h5 class="footer-title">Explore</h5>
                    <?php
                    if($footer_links): ?>
                        <ul class="footer-links">
                        <?php foreach ($footer_links as $page):
                            $page_title = get_the_title($page['links']);
                            $page_link = get_the_permalink($page['links']);?>
                            <li class="footer-link">
                                <a href="<?php echo $page_link; ?>"><?php echo $page_title; ?></a>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif;?>
                </div>
                <div class="_12 _s6 ord_1 ord_s1 part -center">
                    <h3 class="middle-title"><?php echo $footer_text; ?></h3>
                    <a href="<?php _e( home_url( '/' ) ); ?>" id="brand" class="site-logo">
                        <img src="<?php echo $footer_logo; ?>">
                    </a>
                </div>
                <div class="_12 _s2 ord_3 ord_s3 part  -center">
                    <h5 class="footer-title">Contact</h5>
                    <div class="footer-contact-info"><?php echo $contact_info; ?></div>
                </div>
                <div class="_12 _s1 ord_4 ord_s4 part  -center">
                    <div class="footer-social">
                        <?php ornaments_social_network(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="wrap">
                <div class="_12 copyright"><?php if($copyright_text): echo $copyright_text; else: echo 'Copyright &copy; 2019 - <a href="#">Ornaments 3D</a>'; endif; ?></div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>

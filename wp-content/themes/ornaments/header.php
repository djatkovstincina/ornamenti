<?php
/**
 * @package ornaments
 */
global $globalSite;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="<?php echo get_template_directory_uri() . '/bundles/images/favicon.ico'; ?>" type="image/x-icon" />

    <?php wp_head(); ?>
</head>

<body <?php body_class('loading'); ?>>
<?php
$site_logo = get_field('site_logo', 'option');
?>
<div id="page" class="site">
    <div class="m-overlay"></div>
    <div id="loader" class="loader"><div class="logo">
            <svg enable-background="new 0 0 300 300" viewBox="0 0 300 300"><path class="path" stroke="#000000" stroke-width="1" d="m290.7 92.1c-.4 0-.9 0-1.3 0-3.6-.1-7.2-.1-10.7-.3-9.5-.4-19-1.5-28.4.7-1 .3-2 .5-3 .5-5-.2-9.7 2.4-12.1 6.8-.6.8-1.1 1.7-1.7 2.5-.5.8-1.2 1.3-2.1 1.5 1-2 1.8-3.8 2.6-5.5.3-.7 1-1.3.3-2.1-.6-.7-1.4-.6-2.2-.4-3 .5-5.6 2.5-6.9 5.3-.9 1.9-2.1 3.7-3.6 5.2-1.2 1.3-2.4 2.7-3.4 4.1-2.9 4.1-6.3 7.9-10 11.3-.4.4-.6 1.3-1.5.7 0-.5.1-.9.4-1.3 3.2-4.8 6.1-9.9 8.7-15.1.5-1 .8-2.1 1.2-3.1.2-.4.4-.8 0-1.2-.5-.5-1-.3-1.5 0-.5.2-1 .6-1.5.8-5.3 2.6-11 3.8-16.9 3.7-2.5-.1-4.9-.3-7.3-.8-1.7-.5-3.5.2-4.3 1.7-1.2 1.8-2.5 3.5-3.9 5-2.3 3-5 5.7-7.9 8.1 1.3-3.1 3-5.9 5.1-8.5 1.2-1.5 2.3-3.1 3-4.8.2-.5.5-1.1-.1-1.5-.4-.4-.8-.2-1.3-.1-4.6 1.2-9.2 1.9-14 1.9-4.6.7-9.3 1.1-14 1.2-2.6-.3-2.7-.1-2.7 2.6-.1 5.4-1.8 10.6-4.9 15 .8-2.5 1.4-4.6 2.1-6.7 1-2.9 1.3-5.9.8-8.9 0-.1 0-.3 0-.4-.2-1.2-1.4-2.1-2.7-1.8-7.4.1-14.7 1.1-21.8 3-1.5.5-3 1-4.4 1.7-2.2 1.1-2.4 1.1-1.5 3.2.9 2.4 1.4 5 1.4 7.5.1 3.9-.9 7.8-3 11.1-.8-1.7-.1-2.8.2-3.9 1.4-3.8 1.7-7.9.9-11.9-.7-2.3-1-2.7-3.5-2-1.5.4-3 1.1-4.5 1.4-7.2 1.4-12.7 5.9-18.5 9.8-1.6 1-2.2 3-1.5 4.7.9 3.7 1.7 7.4 2.2 11.2.6 2.4.2 5-1.2 7.1-.5-2.3-.8-4.6-.6-7 .2-2.8-.3-5.6-1.5-8.2-1-2.6-2.4-2.8-4.5-.9-2 1.8-3.9 3.7-6 5.4-5.1 3.9-9.9 8-14.5 12.4-1.7 1.5-2.4 3.9-1.7 6.1.2.7.6 1.4.8 2.2 1.2 3.4 1.4 7.1.7 10.6-1.3-.9-2.4-2.2-3-3.6-.9-2-2.5-3.5-4.6-4.2-.5-.2-1-.7-1.5-.1-.4.4-.6 1-.4 1.6.4 1.2.9 2.3 1.4 3.4 1.8 4.5 1 8.1-2.5 10.2-2.1 1.3-2.2 1.3-2.9-1-1.5-4.5-4-8.5-7.4-11.8-3.1-3.1-7-5.2-10.2-8.2-.4-.4-1.1-.6-1.5-.1s-.1 1.1.2 1.5c.5.5 1 .9 1.5 1.3 2 1.8 4 3.6 6 5.4 3.1 3.3 5.4 7.2 7 11.4 1.9 4.3 1.4 5.4-2.2 7.7-1.1.7-1.9 1-2.7-.5-2.3-3.6-5.6-6.4-9.6-8-3.9-2-8.3-2.9-12.6-2.8-3.3.1-6.5.7-9.5 2-.8.3-1.8.8-1.6 1.8.3 1.2 1.4 1.3 2.4 1.2 1.4-.2 2.9-.4 4.3-.5 1.7-.2 3.3-.3 5-.2 7.2.6 14 3.9 18.8 9.3 1.3 1.4.8 2.2-.2 3.4-4.6 5.4-9.5 10.4-14.9 15-2.9 2.3-5.4 5.1-7.3 8.3-.5.9-.7 2.1.4 2.1 2.7 0 5.3 1 7.9.9 1-.1 2-.6 2.7-1.3 2.2-1.4 4-3.2 5.5-5.3 1.2-1.8 2.9-3.1 4.2-4.8.3-.4.8-1 1.4-.8 2 .9 4.4.6 6 2.7 3.7 4.7 7.8 9.2 9.1 15.3.1.5.4.8.9.9.5.1.9-.2 1-.6.4-1.1 1.2-2.3.7-3.5-.9-4-2.6-7.9-4.8-11.4-2.3-3.1-5-5.8-8.1-8-1.5-1-1.5-1.9-.4-3.3.6-1 1.9-1.3 2.9-.6 4.2 1.4 7.8 4 10.5 7.5.4.6.9 1.2 1.6.7.8-.5.4-1.2 0-1.8-.5-.9-1-1.7-1.6-2.5-2.1-2.7-4.5-5-7.3-6.9-.5-.4-1.3-.7-1-1.5s1-1.3 1.8-1.4c1.1-.2 2.3 0 3.3.6 6 2 11.3 5.7 15.4 10.5.5.7 1 1.6 2.5 1-3.6-7.6-10.4-11.2-17.6-14.3.5-1.2 1.5-1.5 2.1-2.1 1.9-2.1 5-2.6 7.4-1.2 2.7 1.1 5.3 2.4 7.8 3.9.6.5 1.3.8 2 1.2.8.3 1.3 0 1.2-1-.3-1.2-.7-2.4-1.3-3.4-.7-1.3-2-2.1-2.9-3.7 3 1.1 5.7 2.7 8 4.8 1.2.9 2.1 2 2.7 3.4.5 1.1 1.3.9 2.1 0 .5-.6.7-1.4.4-2.2-.2-1.1-.4-2.1-.8-3.9.8.9 1.5 1.9 2 2.9.6 1.5.6 3.5 2 4.4 1.5 1 3.5.3 5.3.3.9 0 1.8-.1 2.7-.2 7.3-.6 14.5-1.4 21.8-1.8 1.3-.1 3.2.6 3.7-.7.5-1.5-1.4-2.3-2.4-3.3-2-1.9-4.2-3.7-6.3-5.6 4.2 1.7 8 4.1 11.4 7 .9 1.1 2.2 1.9 3.7 2.2 3 .3 6.1.2 9-.4 2.2-.9 4.5-1.5 6.8-1.8 6.9-.3 13.3-2.7 20-4.2 1 0 1.8-.6 2.2-1.5-1.9-2.9-5.6-3.9-7.8-6.6 4 .7 7.6 2.8 10.2 6 .1.1.2.3.3.4.8.9 2.3 1 3.2.2 2.1-1.3 4.5-2.1 6.9-2.3 1.4-.2 2.8-.7 4.1-1.3 1.7-.8 1.8-1.7.3-2.9-1.5-1.3-2.6-2.9-3.4-4.7 1.6.8 2.9 2 3.9 3.4.9 1 1.6 2.1 2.6 3.1 1.9 2.6 5.1 3.9 8.3 3.2.9-.1 1.8-.2 2.7-.1 5.9.1 11.9-.5 17.7-1.6.7-.2 1.8-.1 2-1 .4-1.6 1.6-1.6 2.8-1.9 2.3-.6 4.6-1 6.9-1.3.9-.1 1.8-.3 2.6-.6 1.9-.8 2.1-1.6 1.1-3.4-3.5-5.8-6.4-12.1-12.6-16.1 1.2-.4 1.7.1 2.4.5 5.9 3.4 10.5 8.6 13.2 14.8 1.3 2.7 1.5 2.8 4.2 1.5 5.4-2.6 11-5.1 16.3-8 4.4-2.4 8.4-5.6 11.9-9.2 1.7-1.9 1.6-3.3-.6-4.6-1.3-.8-2.8-1.4-4.1-2.2-4-2.5-8.3-4.5-12.8-5.8-.7-.2-1.5-.1-1.9-.9.7-.3 1.4-.3 2.1-.1 7.2 1.1 14.3 2.6 21.3 4.7 1.4.5 2.7.7 3.6-.7 2-3 5-5.1 7-8.1 2.8-4 5.2-8.3 7.2-12.8 3.3-8.3 7.9-16 10.8-24.5 2-4.9 1.4-6-3.8-6.5zm-230 80.9c.4.5 0 1.1-.5 1.4-.8.4-1.5.9-2.9 1.6-.1-3-.4-6-1.1-8.9 1.8 1.7 3.3 3.7 4.5 5.9zm-46.3 4c7.9-3.4 22.9 2.9 23.7 9.8-6.1-7.4-14.1-9.7-23.7-9.8zm27.2 25.6c5 3.5 8.7 8.6 10.2 14.6-2.7-2.6-4.2-6-6.7-8.8-1.3-2.1-3.5-3.5-5.9-3.9-.8 0-1.6-.5-1.3-1.5 0-.1.1-.2.1-.3.4-.9 1.4-1.3 2.2-1 .5.2 1 .5 1.4.9zm7.3-5.5c-1.9-.3-3.7-1.4-4.8-3 1.9.2 3.7 1.3 4.8 3zm-5.3-5.2c-1.9 1.8-3.5 4-4.7 6.4-3.2 5-6.9 9.7-10.9 14.1-.7.8-1.4 1.5-2.2 2.1-1.3 1.4-3.4 1.9-5.2 1.1-1.1-.6-2.5 0-3.8-.8 1.7-3.2 4.1-6.1 6.9-8.4 4.2-3.5 8.1-7.4 11.7-11.5 2.4-2.9 4.9-5.5 7.7-7.9 2.9-2.4 6.1-4.6 9.4-6.4 2.1-1.2 4.3-2.4 6.3-3.8 1-.8 2.3-1.4 3.7-1.5.3 0 .8 0 .9-.1 4.1-3.9 9.6-5.1 14.3-7.8 5.4-3.1 11-5.9 16.8-8.3 5-2.1 10-3.9 15.1-5.5 5.3-1.6 10.6-3.5 16.1-4.7 7.9-1.7 15.9-2.8 24-3.2-2.2.9-4.4 1.4-6.7 1.5-8.7 1.3-17.3 3.5-25.6 6.6-10.2 3.8-20.1 8-29.9 12.8-6.2 2.9-12.1 6.3-18.1 9.4-2.1 1.1-4.2 2.7-6.9 1.7-.2-.1-.4-.1-.7-.1-.5.1-.7.6-.6 1.1.4 1.6-.8 1.8-1.8 2.3-5.3 3.2-10.7 6.9-15.8 10.9zm25.5-5.7-.3.6-8.5-3.7c3.3-2.3 3.3-2.3 5.5-.1 1.1 1 2.2 2.1 3.3 3.2zm9.6.4c-3.8-2.7-7.8-5-11.9-7.1 1.8-1.5 3.9-2.7 6.2-3.5.9-.3 1.4.3 2 .8 2.5 2.8 3.8 6.2 3.7 9.8zm1.8-3.6c-.7-.9-1.1-2.1-1.1-3.2 1.4.7 1.3 1.7 1.1 3.2zm212.2-84.4c-2.3 6.8-6.2 12.9-8.7 19.6-2.3 6.1-5.3 11.8-9.1 17.1-1.8 2.5-3.7 4.8-5.9 6.9-.8.9-2.1 1.2-3.2.7-9.1-2.8-18.5-4.4-28-4.8-2.2-.2-4.4 0-6.5.7.2 1 1.1 1.6 2.1 1.6 3.7.5 7.3 1.2 10.8 2.3 4.7 1.3 9 3.3 13 6.2.9.6 1.9 1.2 2.8 1.8 1 .6 1.1 1.4.3 2.2-1.1 1.1-2.2 2.2-3.5 3.2-3.5 3-7.3 5.5-11.4 7.6-4 1.7-8 3.6-11.7 5.8-1.1.6-1.7.5-2.3-.8-2.6-5.7-6.8-10.6-12-14.2-1.9-1.2-3.9-2.2-6-2.9-4-1.7-7.8-3.8-12-5.2-.5-.2-1.1-.4-1.4.3-.1.4 0 .9.4 1.2s.7.5 1.1.7c3.7 2.2 7.5 4.4 11.2 6.8 3.1 1.9 5.6 4.5 7.6 7.5 1.5 2.5 2.8 5.1 4.4 7.5.9 1.4.2 1.8-1 2.1-3 .6-6 1-8.9 1.7-1.3.3-1.8-.4-2.3-1.4-1.5-3.2-3.4-6.3-5.6-9.1-1.5-2-3.1-3.8-4.9-5.5-.5-.5-1.3-1.4-2-.7-.8.8.1 1.7.8 2.2 2.5 2 4.5 4.4 6 7.2 1.5 2.4 2.7 4.9 3.7 7.5.9 2.4.8 2.4-1.8 3-4.7.8-9.6 1.2-14.4 1.2-1.1-.1-2.2 0-3.4.1-2.7.5-5.4-.7-7-3-5-5.7-10.7-10.8-16.9-15.3-3-1.9-6.3-3.2-9.8-3.8-.2 1.8 1 2.3 2 2.9 4 2.5 7.8 5.3 11.4 8.2 2.4 2.2 4.5 4.7 6.2 7.5.9 1.3.7 2.1-.9 2.4-2.9.5-5.7 1.3-8.4 2.3-.8.4-1.3-.1-1.7-.6-2.3-2.3-5.1-4-8.2-5-4.8-2.1-9.5-4.5-14.4-6.5-4-1.6-8.2-2.9-12.4-4.3-2.5-.9-5.1-1.7-7.7-2.4-.8-.2-2.3-.8-2.5.6-.1 1.1 1.3 1.4 2.3 1.5 3.3.3 6.4 1.3 9.2 3.1.5.3 1 .5 1.5.7 5.7 1.7 11.1 4.3 16 7.6 3.3 2 6.5 4.3 9.5 6.7-2.6 1.1-5.3 1.9-8.1 2.5-3.3 1.2-6.7 1.8-10.2 1.9-2.2.1-4.4.7-6.5 1.7-3.1 1.1-6.4 1.2-9.6.5-.9-.2-1.7-.6-2.3-1.3-5.7-4.5-12-8.3-18.7-11.1-2.4-1.2-4.7-2.7-6.6-4.5-.5-.4-1.1-.8-1.6-1.2s-1-.5-1.5 0c-.1.1-.1.1-.2.2-.3.5-.2 1.2.3 1.5 1.8 1.6 3.7 3.3 5.7 4.8 3.9 2.9 7.8 5.6 11.8 8.3 1.2.8 2.3 1.9 3.2 3.1-1.4 1.4-3.2 1-4.7.9-4.3-.1-8.5.8-12.7 1.1-2.8.2-5.6.5-8.4.7-2 .4-3.9-.8-4.5-2.7-1.1-2.1-1.9-4.3-2.3-6.6-.2-2-2-3.1-3.2-4.6-1.8-2.3-1.7-2.4.9-3.9 10.8-6 22-11.1 33.5-15.5 6-2.4 12.2-4.6 18.4-6.3 5.7-1.3 11.4-2.3 17.2-2.9 2.7-.3 5.3-.9 7.9-1.6 8.3-1.9 16.7-3.1 25.2-3.5 13.2-1.1 26.3-3.1 39.2-5.7 5.5-1.5 10.9-3.3 16.2-5.4 5.4-1.9 10.4-4.6 15.1-8 3.1-1.9 6.4-3.7 9.8-5.2 1.5-.6 2.8-1.4 4-2.4 4.1-3.6 8.5-6.9 13.2-9.7 1-.6 1.8-1.4 2.4-2.3.1-.3.1-.6-.1-.9s-.6-.4-.9-.2-.6.4-.8.6c-2.8 2.5-5.8 4.6-9 6.5-.6.3-1.1.8-1.6 1.2-4.8 4.2-10.2 7.7-16.1 10.2-2.3 1.2-4.3 2.8-6.5 4.2-5.1 2.9-10.4 5.2-16 6.9-6.7 2.5-13.7 4.3-20.8 5.4-9.2 1.6-18.4 2.5-27.7 2.7-4.6.2-9.1.8-13.6 1.7-7.8 1.1-15.7 1-23.4 2-6.3.8-12.7 1.5-18.9 2.9-11.1 2.6-22 6.1-32.5 10.6-6.7 2.8-13.1 6.3-19.6 9.7-2.6 1.4-5.2 2.7-7.8 3.9-1.9.8-2.6.4-2.3-1.7.4-3.3 0-6.8-1.3-9.9-1-2.2-.1-4.9 1.9-6.2 4.3-4 8.9-7.8 13.7-11.4 1.6-1.3 3-2.7 4.4-4.2 2.1-1.9 3.1-1.6 4.1 1 .3.7.5 1.5.5 2.3-.2 4.3.9 8.5.5 12.7 0 .3-.1.7 0 1 .1 1-.1 2.3 1.1 2.5 1.1.2 1.2-1.2 1.4-2 1.7-3.4 2.5-7.2 2.3-11-.4-3.7-1-7.3-1.9-10.9-.7-1.7 0-3.7 1.6-4.6 6.5-4.2 12.8-8.6 20.8-9.7 1.3-.2 1.9.1 2.1 1.4.5 3 .2 6.1-.8 8.9-1.3 3.9-1.6 8.1-2.7 12.1-.2.5 0 1.2.5 1.4.5.2 1.1 0 1.3-.6 4-6.6 7.4-13.3 6.8-21.3-.2-1.3-.5-2.6-.9-3.9-.5-2.1-.4-2.5 1.7-3.2 3.4-1.2 6.8-2.2 10.3-2.9 3.8-.5 7.6-.8 11.4-.9 2.7-.1 2.8.2 2.7 2.9.1 2.6-.4 5.1-1.5 7.5-1.1 2.5-1.8 5.1-2 7.8-.2 2.8-1.2 5.5-2.9 7.7-.3.4-.6.9-.8 1.5-.2.5-.4 1.1.3 1.5.4.2.9 0 1.2-.4 1-.9 1.9-1.9 2.6-3.1 2-4.1 4.6-7.9 6.3-12.2 1.3-3.7 2.3-7.5 2.8-11.4.1-.9.3-1.4 1.4-1.4 8-.2 15.8-1.9 23.7-2.1h1.2c.2.5.2 1-.1 1.4-4.1 5.9-8.2 11.7-12.3 17.6-1.7 1.6-3.3 3.3-4.8 5.2-.3.6-1 1.1-.4 1.8.5.6 1.3.2 1.9 0 1.7-.8 3.2-2.1 4.3-3.7 4.6-5.2 9.7-10 14.3-15.2 1.8-2.1 3.6-4.3 5.3-6.5.9-1.4 2.5-2 4.1-1.5 7.1 1.6 14.6 1 21.4-1.8.5-.2 1-.5 1.7.1-1.3 3.2-2.9 6.2-4.7 9.1-2.9 4.8-6.1 9.3-9.2 14-.8 1.2-1.8 2.3-2.8 3.3-.4.5-.5 1.1-.1 1.5s1.1.4 1.5 0c1-.9 2-1.8 2.9-2.8 3.3-3.2 6.7-6.2 9.9-9.6 4.9-5.1 9-10.8 13.6-16.2 1-1.3 2-2.7 2.9-4.1.6-1.3 1.8-2.2 3.3-2.4.1 1.2-.3 2.3-1 3.3-1.5 2.3-2.9 4.6-4.5 6.7-2.6 3.3-4.7 7.1-6.1 11.1-1.1 3-3.5 5.5-4.4 8.7-.3 1.1-1.8 2.5-.5 3.2 1.4.7 1.7-1.4 2.2-2.3 2.6-5 5.6-10 8.7-14.7 3.2-4.1 6.7-8.1 10-12.2 1.4-1.6 2.7-3.3 4.1-4.9 1.1-1.2 2.5-1.9 4.1-2.1 3.7-.6 7.3-.9 10.9-1.7 4.3-.6 8.7-.7 13-.4 4.1 0 8.3.2 12.4.6 3.1.2 6.3.1 9.4.2 4.8-.1 4.9 0 3.5 4.2z"/></svg>
        </div></div>
    <header id="masthead" class="site-header" role="banner">
        <div class="wrapper-fluid">
            <div class="wrap top-header">
                <div class="lang-switch"><?php do_action('wpml_add_language_selector'); ?></div>
                <div class="top-menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu' => 'top-menu' ) ); ?>
                </div>
            </div>
            <div class="wrap bottom-header">
                <a href="<?php _e( home_url( '/' ) ); ?>" id="brand" class="site-logo mobile">
                    <?php
                    if ($site_logo): echo '<img src="' . $site_logo . '" >';
                    else: echo '3D Ornaments';
                    endif; ?>
                </a>
                <div class="right-nav ls-m">
                    <div class="search-trigger"><a href="javascript:void(0);"><i class="fa fa-search"></i></a></div> |
                </div>
                <button id="menu-button" class="site-menu-button v2">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <nav id="site-navigation" class="site-navigation" role="navigation">
                    <a href="<?php _e( home_url( '/' ) ); ?>" id="brand" class="site-logo desktop">
                        <?php
                        if ($site_logo): echo '<img src="' . $site_logo . '" >';
                        else: echo '3D Ornaments';
                        endif; ?>
                    </a>
                    <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
                    <div class="right-nav ls-d">
                        <div class="search-trigger"><a href="javascript:void(0);"><i class="fa fa-search"></i></a></div>
                    </div>
                </nav>
                <div class="_12 header-search-form">
                    <form role="search" class="header-form search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                        <div class="search-elements">
                            <label class="screen-reader-text" for="q">Search</label>
                            <input type="search" class="header-form-search" placeholder="Type keyword and press Enter to search" value="" name="s">
                            <input type="submit" class="search-submit" value="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
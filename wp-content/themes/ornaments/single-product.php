<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ornaments
 */

get_header();
$product_id = get_field('product_id');
$product_description = get_field('product_description');
$popup_desc = get_field('popup_description'); ?>
    <div id="content" class="site-content">
        <?php
        $header_height = get_field('h_height');
        $random_image = get_template_directory_uri() . '/src/images/random.jpg';
        $header_control = get_field('h_control');
        $header_video_mp4 = get_field('h_mp4');
        $header_video_webm = get_field('h_webm');
        $header_video_ogv = get_field('h_ogv');
        $header_image_num = get_field('h_slider' );
        $header_external = get_field('h_external');
        $embed = get_field('external_video');
        if ( $embed ) {
            preg_match('/src="(.+?)"/', $embed, $matches);
            $src = $matches[1];
            $params = array(
                'controls'    => 1,
                'hd'        => 1,
                'rel' => 0,
                'autohide'    => 1,
                'autoplay' => 0,
                'showinfo' => 0,
            );
            $new_src = add_query_arg($params, $src);
            $embed = str_replace($src, $new_src, $embed);
            $attributes = 'frameborder="0"';
            $embed = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $embed);
        }
        $count = $header_image_num; ?>

        <section id="header-section" class="header-section<?php if ( !$header_control ) echo ' wrapper-fluid'; else echo ' wrapper'; if ( $header_height === 'Full Screen' ) echo ' full'; ?>">
            <?php
            if ( $header_control ) :
                if ( $header_external ) : ?>
                    <div class="wrap video-container -external">
                        <?php echo $embed; ?>
                    </div>
                <?php
                else: ?>
                    <div class="wrap video-container">
                        <video id="header-video" data-object="video" class="site-header-video" poster="<?php echo $random_image; ?>" autoplay playsinline muted loop>
                            <?php
                            if ( $header_video_ogv ) : ?>
                                <source src="<?php echo $header_video_ogv; ?>" type="video/ogv">
                            <?php
                            endif;
                            if ( $header_video_webm ) : ?>
                                <source src="<?php echo $header_video_webm; ?>" type="video/webm">
                            <?php
                            endif; ?>
                            <source src="<?php echo $header_video_mp4; ?>" type="video/mp4">
                        </video>
                    </div>
                <?php
                endif;
            elseif ( have_rows('h_slider') ): ?>
                <div class="wrap header-slider border-img">
                    <?php while ( have_rows('h_slider') ) : the_row();
                        $header_image = get_sub_field('h_image');
                        $black = get_sub_field('bb');
                        $header_title = get_sub_field('h_title');
                        $header_subtitle = get_sub_field('h_subtitle'); ?>
                        <div class="header-slide <?php if($count==1) echo 'inner'; ?>" data-image="<?php echo $header_image; ?>" style="background-image: url(<?php echo $header_image; ?>);">
                            <div class="inner-image animated-image" style="background-image: url(<?php echo $header_image; ?>);"></div>

                            <div class="wrapper">
                                <div class="header-info _12 <?php if($black) echo 'black'; ?>">
                                    <?php
                                    if ( $header_title ) : ?>
                                        <h1 class="header-title">
                                            <?php
                                            echo $header_title; ?>
                                        </h1>
                                    <?php
                                    endif;
                                    if ( $header_subtitle ) : ?>
                                        <p class="header-subtitle">
                                            <?php
                                            echo $header_subtitle; ?>
                                        </p>
                                    <?php
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile; ?>
                </div>
            <?php
            elseif( has_post_thumbnail() ): ?>
                <div class="wrap header-slider">
                    <div class="header-slide" data-image="<?php the_post_thumbnail_url('large'); ?>" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>);">
                        <div class="inner-image animated-image" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>);"></div>
                    </div>
                </div>
            <?php
            else : ?>
                <div class="wrap header-image" style="background-image: url(<?php echo $random_image; ?>);"></div>
            <?php
            endif;
            ?>
        </section>
        <div class="section-social">
            <div class="header-social">
                <?php echo ornaments_social_network(); ?>
            </div>
        </div>
        <main id="main" class="page-main site-main" role="main">
            <?php
            the_breadcrumb(); ?>
            <section class="section single-prod">
                <div class="wrapper">
                    <div class="wrap">
                        <div class="_12 title">
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <div class="wrap">
                        <div class="_12 _s7 _m6">
                            <div class="images-wrap">
                                <?php
                                if (have_rows('image_repeater')): ?>
                                    <div class="main-image  js-main-image">
                                    <?php while (have_rows('image_repeater')) : the_row();
                                        $product_image = get_sub_field('product_image');
                                        $id = get_the_ID(); ?>
                                        <a class="i-link border-img" data-fancybox data-touch="false"  data-src="#get-popup-<?php echo $id; ?>" href="javascript:;">
                                            <img class="p-image" src="<?php echo $product_image; ?>">
                                            <div class="glow-wrap">
                                                <i class="glow"></i>
                                            </div>
                                        </a>
                                    <?php
                                    endwhile; ?>
                                    </div>
                                    <div class="thumbs">
                                    <?php while (have_rows('image_repeater')) : the_row();
                                        $product_image = get_sub_field('product_image');
                                        $id = get_the_ID(); ?>
                                        <a class="i-link border-img" data-fancybox data-src="#get-popup-<?php echo $id; ?>" href="javascript:;">
                                            <img class="p-image" src="<?php echo $product_image; ?>">
                                            <div class="glow-wrap">
                                                <i class="glow"></i>
                                            </div>
                                        </a>
                                    <?php
                                    endwhile; ?>
                                    </div>

                                    <div id="get-popup-<?php echo $id; ?>" class="product-popup wrapper">
                                        <div class="thumbs-popup">
                                            <?php while (have_rows('image_repeater')) : the_row();
                                                $product_image = get_sub_field('product_image'); ?>

                                                <img class="p-image" src="<?php echo $product_image; ?>">
                                            <?php
                                            endwhile; ?>
                                        </div>
                                        <?php if($popup_desc): ?>
                                            <div class="description"><?php echo $popup_desc; ?></div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="_12 _s5 _m6">
                            <?php _e('Product code: ', 'ornaments'); ?>
                            <div class="product-id"><?php echo $product_id; ?></div>
                            <?php _e('Category: ', 'ornaments'); ?>
                            <div class="product-id">
                                <?php
                                $terms = get_the_terms( $post->ID, 'product_category' );
                                foreach($terms as $term) { echo $term->name; } ?>
                            </div>
                            <div class="product-desc"><?php echo $product_description; ?></div>
                        </div>
                    </div>
                    <div class="wrap">
                        <div class="_12">
                            <ul class="tabs">
                                <li class="tab" data-attribute="1"><?php _e('Specification', 'ornaments')?></li>
                                <li class="tab" data-attribute="2"><?php _e('Description', 'ornaments')?></li>
                                <li class="tab" data-attribute="3"><?php _e('Order', 'ornaments')?></li>
                            </ul>
                        </div>
                        <div class="_12">
                            <div id="1" class="container specification">
                                <?php
                                if (have_rows('spec_repeater')): ?>
                                    <div class="spec-table">
                                    <?php while (have_rows('spec_repeater')) : the_row();
                                        $spec_item = get_sub_field('spec_item');
                                        $value = get_sub_field('value'); ?>
                                        <div class="spec-row">
                                            <div class="half left"><?php echo $spec_item; ?></div>
                                            <div class="half right"><?php echo $value; ?></div>
                                        </div>
                                    <?php
                                    endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div id="2" class="container description">
                                <div class="description">
                                    <?php
                                    $desc = get_field('description');
                                    echo $desc; ?>
                                </div>
                            </div>
                            <div id="3" class="container order">
                                <div class="order-form">
                                    <?php
                                    $form = do_shortcode('[contact-form-7 id="538" title="Order a Product Form"]');
                                    $form = str_replace('post_title', $post->post_title, $form);
                                    echo $form; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper-fluid">
                    <div class="wrap">
                        <div class="_12 similar-title">
                            <h3 class="title center"><?php _e('Other products', 'ornaments')?></h3>
                        </div>
                        <div class="_12">
                            <?php
                            if (have_rows('similar_products')): ?>

                                <div class="similar-products">
                                    <?php while (have_rows('similar_products')) : the_row();
                                        $similar_prod = get_sub_field('similar_prod');
                                        ?>

                                        <div class="similar-single">
                                            <div class="title"><?php echo $similar_prod->post_title; ?></div>
                                            <div class="similar-image">
                                                <a class="link" href="<?php echo get_permalink($similar_prod->ID); ?>">
                                                <?php
                                                setup_postdata($similar_prod);
                                                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($similar_prod), 'large' );
                                                    if( $thumb ):
                                                        echo '<div class="border-img image" style="background-image:url(' . $thumb[0] . ')"></div>';
                                                    else:
                                                        echo '<div class="border-img image" style="background-image:url(http://placehold.it/300x200)"></div>';
                                                    endif;
                                                wp_reset_postdata();
                                                ?>
                                                </a>
                                            </div>
                                        </div>
                                    <?php
                                    endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <?php
            if (have_rows('blocks_gallery')): ?>
                <section class="section gallery">
                    <div class="wrapper-fluid">
                        <?php while (have_rows('blocks_gallery')) : the_row();
                            $title = get_sub_field('title');
                            $text = get_sub_field('text'); ?>

                            <div class="wrap ing-wrap">
                            <div class="_12 _m4 title-wrap">
                                <h4 class="title"><?php echo $title; ?></h4>
                                <div class="text"><?php echo $text; ?></div>
                            </div>
                            <?php if (have_rows('images_gallery')): ?>
                                <div class="_12 _m8 images-wrap">
                                    <?php while (have_rows('images_gallery')) : the_row();
                                        $image = get_sub_field('image'); ?>
                                        <img class="gallery-image" src="<?php echo $image; ?>"/>
                                    <?php endwhile; ?>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </section>
            <?php
            endif;

            $section_title = get_field('section_title');
            if( have_rows('card_repeater') ): ?>
                <section class="section categories">
                    <div class="wrapper-fluid">
                        <div class="wrap">
                            <h2 class="categories-title _12 _m8 ofs_m2"><?php if($section_title) echo $section_title; ?></h2>
                        </div>
                        <div class="wrap">
                            <?php while ( have_rows('card_repeater') ) : the_row();
                                $card_image = get_sub_field('card_image');
                                $card_title = get_sub_field('card_title');
                                $card_link = get_sub_field('card_link'); ?>

                                <a href="<?php echo $card_link; ?>" class="link">
                                    <div class="category-image" style="background-image: url('<?php echo $card_image; ?>')">
                                        <div class="title-wrap">
                                            <h4 class="title"><?php echo $card_title; ?></h4>
                                        </div>
                                    </div>
                                    <span class='line-1'></span>
                                    <span class='line-2'></span>
                                    <span class='line-3'></span>
                                    <span class='line-4'></span>
                                </a>
                            <?php
                            endwhile; ?>
                        </div>
                    </div>
                </section>
            <?php
            endif; ?>
        </main>
    </div>
<?php
get_footer();